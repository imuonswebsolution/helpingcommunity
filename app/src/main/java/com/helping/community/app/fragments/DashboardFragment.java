package com.helping.community.app.fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.helping.community.app.R;
import com.helping.community.app.models.DashboardDataModel;
import com.helping.community.app.models.DashboardResponseModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.Utils;
import com.helping.community.app.utils.ViewUtils;
import com.helping.community.app.views.DashboardActivity;
import com.helping.community.app.views.SplashActivity;


import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardFragment extends Fragment {

    private TextView mTextDirectUsers, mTextRepurchaseBinaryIncome, mTextRightBV, mTextLeftBV,
            mTextBinaryIncome, mTextDirectIncome, mTextTotalIncome, mTextTotalWithdrawal,text_distributor_income,text_silver_income,text_gold_income,text_platinum_income,text_diamond_income,text_repurchase_binary_income;


    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        return fragment;
    }

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        initializeViews(view);


        if (Utils.checkInternetConnection(DashboardFragment.this.getContext())) {
            getDashboardData();
        } else {
            Toast.makeText(DashboardFragment.this.getContext(), getString(R.string.no_internet_connection_message),
                    Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    private void getDashboardData() {
        final ProgressDialog pd = ViewUtils.getProgressBar(DashboardFragment.this.getContext(), "Loading...", "Please wait..!");
        Map<String, String> loginMap = new HashMap<>();

        HelpingServices apiService = ApiHandler.getApiService();
        final Call<DashboardResponseModel> loginCall = apiService.wsGetDashboardData("Bearer " + SharedPreferenceUtils.getAccesstoken(DashboardFragment.this.getContext()) ); //SharedPreferenceUtils.getLoginObject(DashboardFragment.this.getContext()).getData().getAccess_token()
        loginCall.enqueue(new Callback<DashboardResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<DashboardResponseModel> call,
                                   Response<DashboardResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    DashboardResponseModel loginModel = response.body();
                    if (loginModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            loginModel.getStatus().equals("OK")) {
                        setDashboardData(loginModel.getDashboardDataModel());
                        SharedPreferenceUtils.storeDashboardObject(loginModel,
                                DashboardFragment.this.getContext());
                        SharedPreferenceUtils.storeUserId(DashboardFragment.this.getContext(), loginModel.getDashboardDataModel().getUserId());
                    } else {
                        Toast.makeText(DashboardFragment.this.getContext(), loginModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<DashboardResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(DashboardFragment.this.getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setDashboardData(DashboardDataModel data) {

        int a = data.getWorkingWalletBalance();
        data.setWorkingWalletBalance(a);

        mTextDirectUsers.setText(data.getTotalPh().toString());
        text_distributor_income.setText(data.getTotalGh().toString());
        text_silver_income.setText(data.getPendingPh().toString());
        text_gold_income.setText(data.getPendingGh().toString());


    }

    private void initializeViews(View view) {
        mTextDirectUsers = view.findViewById(R.id.text_total_ph_value);
        text_distributor_income = view.findViewById(R.id.text_total_gh_value);
        text_silver_income = view.findViewById(R.id.text_pending_ph_value);
        text_gold_income = view.findViewById(R.id.text_pending_gh_value);

    }

}
