package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Zero {

    @SerializedName("showtime")
    @Expose
    private String showtime;
    @SerializedName("msgdata")
    @Expose
    private List<Msgdatum> msgdata = null;

    public String getShowtime() {
        return showtime;
    }

    public void setShowtime(String showtime) {
        this.showtime = showtime;
    }

    public List<Msgdatum> getMsgdata() {
        return msgdata;
    }

    public void setMsgdata(List<Msgdatum> msgdata) {
        this.msgdata = msgdata;
    }
}
