package com.helping.community.app.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.helping.community.app.R;

public class ThankYou extends AppCompatActivity {
    String userid_str;
    TextView userid;
    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);

      userid_str = getIntent().getStringExtra("userid");

      userid = findViewById(R.id.userid);
      login = findViewById(R.id.login);

      userid.setText(userid_str);

      login.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent dash = new Intent(ThankYou.this,DashboardActivity.class);
              startActivity(dash);
          }
      });
    }
}
