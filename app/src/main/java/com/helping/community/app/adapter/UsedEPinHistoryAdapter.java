package com.helping.community.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.helping.community.app.R;
import com.helping.community.app.models.UsedEPinDataModel;

import java.util.ArrayList;
import java.util.List;

public class UsedEPinHistoryAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    List<UsedEPinDataModel> mHistoryList;

    public UsedEPinHistoryAdapter(Context context, List<UsedEPinDataModel> list) {
        mContext = context;
        mHistoryList = new ArrayList<UsedEPinDataModel>();
        mHistoryList.addAll(list);
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mHistoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.layout_used_epin_history_list_item, null);
        }
        holder = new ViewHolder();
        final UsedEPinDataModel details = mHistoryList.get(position);

        holder.ePin = view.findViewById(R.id.text_epin);
        holder.productName = view.findViewById(R.id.text_product_name);
        holder.usedBy = view.findViewById(R.id.text_used_by);
        holder.usedDate = view.findViewById(R.id.text_used_date);
        holder.status = view.findViewById(R.id.text_status);

        holder.ePin.setText(details.getPin());
        holder.productName.setText(details.getProduct_name());
        holder.usedBy.setText(details.getUsed_by_userId());
        holder.usedDate.setText(details.getUsed_date().split(" ")[0]);
        holder.status.setText(details.getStatus());

        return view;
    }

    public static class ViewHolder {
        TextView ePin, productName, usedBy, usedDate, status;
    }
}
