
package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashboardDataModel {

    @SerializedName("coin")
    @Expose
    private Integer coin;
    @SerializedName("btc")
    @Expose
    private Integer btc;
    @SerializedName("usd")
    @Expose
    private Integer usd;
    @SerializedName("total_investment")
    @Expose
    private Integer totalInvestment;
    @SerializedName("active_investment")
    @Expose
    private Integer activeInvestment;
    @SerializedName("total_withdraw")
    @Expose
    private Integer totalWithdraw;
    @SerializedName("total_profit")
    @Expose
    private Integer totalProfit;
    @SerializedName("direct_income")
    @Expose
    private Integer directIncome;
    @SerializedName("direct_income_withdraw")
    @Expose
    private Integer directIncomeWithdraw;
    @SerializedName("direct_income_balance")
    @Expose
    private Integer directIncomeBalance;
    @SerializedName("level_income")
    @Expose
    private Integer levelIncome;
    @SerializedName("level_income_withdraw")
    @Expose
    private Integer levelIncomeWithdraw;
    @SerializedName("level_income_balance")
    @Expose
    private Integer levelIncomeBalance;
    @SerializedName("roi_income")
    @Expose
    private Integer roiIncome;
    @SerializedName("roi_income_withdraw")
    @Expose
    private Integer roiIncomeWithdraw;
    @SerializedName("roi_income_balance")
    @Expose
    private Integer roiIncomeBalance;
    @SerializedName("binary_income")
    @Expose
    private Integer binaryIncome;
    @SerializedName("binary_income_withdraw")
    @Expose
    private Integer binaryIncomeWithdraw;
    @SerializedName("binary_income_balance")
    @Expose
    private Integer binaryIncomeBalance;
    @SerializedName("top_up_wallet")
    @Expose
    private Integer topUpWallet;
    @SerializedName("top_up_wallet_withdraw")
    @Expose
    private Integer topUpWalletWithdraw;
    @SerializedName("top_up_Wallet_balance")
    @Expose
    private Integer topUpWalletBalance;
    @SerializedName("transfer_wallet")
    @Expose
    private Integer transferWallet;
    @SerializedName("transfer_wallet_withdraw")
    @Expose
    private Integer transferWalletWithdraw;
    @SerializedName("transfer_Wallet_balance")
    @Expose
    private Integer transferWalletBalance;
    @SerializedName("working_wallet")
    @Expose
    private Integer workingWallet;
    @SerializedName("working_wallet_withdraw")
    @Expose
    private Integer workingWalletWithdraw;
    @SerializedName("working_Wallet_balance")
    @Expose
    private Integer workingWalletBalance;
    @SerializedName("single_leg_balance")
    @Expose
    private Integer singleLegBalance;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("total_ph")
    @Expose
    private Integer totalPh;
    @SerializedName("total_give_amount")
    @Expose
    private String totalGiveAmount;
    @SerializedName("pending_ph")
    @Expose
    private String pendingPh;
    @SerializedName("total_gh")
    @Expose
    private String totalGh;
    @SerializedName("total_get_amount")
    @Expose
    private Integer totalGetAmount;
    @SerializedName("pending_gh")
    @Expose
    private Integer pendingGh;
    @SerializedName("ph_count")
    @Expose
    private Integer phCount;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("ref_url")
    @Expose
    private String refUrl;
    @SerializedName("gh_release_date")
    @Expose
    private String ghReleaseDate;

    public Integer getCoin() {
        return coin;
    }

    public void setCoin(Integer coin) {
        this.coin = coin;
    }

    public Integer getBtc() {
        return btc;
    }

    public void setBtc(Integer btc) {
        this.btc = btc;
    }

    public Integer getUsd() {
        return usd;
    }

    public void setUsd(Integer usd) {
        this.usd = usd;
    }

    public Integer getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(Integer totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

    public Integer getActiveInvestment() {
        return activeInvestment;
    }

    public void setActiveInvestment(Integer activeInvestment) {
        this.activeInvestment = activeInvestment;
    }

    public Integer getTotalWithdraw() {
        return totalWithdraw;
    }

    public void setTotalWithdraw(Integer totalWithdraw) {
        this.totalWithdraw = totalWithdraw;
    }

    public Integer getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(Integer totalProfit) {
        this.totalProfit = totalProfit;
    }

    public Integer getDirectIncome() {
        return directIncome;
    }

    public void setDirectIncome(Integer directIncome) {
        this.directIncome = directIncome;
    }

    public Integer getDirectIncomeWithdraw() {
        return directIncomeWithdraw;
    }

    public void setDirectIncomeWithdraw(Integer directIncomeWithdraw) {
        this.directIncomeWithdraw = directIncomeWithdraw;
    }

    public Integer getDirectIncomeBalance() {
        return directIncomeBalance;
    }

    public void setDirectIncomeBalance(Integer directIncomeBalance) {
        this.directIncomeBalance = directIncomeBalance;
    }

    public Integer getLevelIncome() {
        return levelIncome;
    }

    public void setLevelIncome(Integer levelIncome) {
        this.levelIncome = levelIncome;
    }

    public Integer getLevelIncomeWithdraw() {
        return levelIncomeWithdraw;
    }

    public void setLevelIncomeWithdraw(Integer levelIncomeWithdraw) {
        this.levelIncomeWithdraw = levelIncomeWithdraw;
    }

    public Integer getLevelIncomeBalance() {
        return levelIncomeBalance;
    }

    public void setLevelIncomeBalance(Integer levelIncomeBalance) {
        this.levelIncomeBalance = levelIncomeBalance;
    }

    public Integer getRoiIncome() {
        return roiIncome;
    }

    public void setRoiIncome(Integer roiIncome) {
        this.roiIncome = roiIncome;
    }

    public Integer getRoiIncomeWithdraw() {
        return roiIncomeWithdraw;
    }

    public void setRoiIncomeWithdraw(Integer roiIncomeWithdraw) {
        this.roiIncomeWithdraw = roiIncomeWithdraw;
    }

    public Integer getRoiIncomeBalance() {
        return roiIncomeBalance;
    }

    public void setRoiIncomeBalance(Integer roiIncomeBalance) {
        this.roiIncomeBalance = roiIncomeBalance;
    }

    public Integer getBinaryIncome() {
        return binaryIncome;
    }

    public void setBinaryIncome(Integer binaryIncome) {
        this.binaryIncome = binaryIncome;
    }

    public Integer getBinaryIncomeWithdraw() {
        return binaryIncomeWithdraw;
    }

    public void setBinaryIncomeWithdraw(Integer binaryIncomeWithdraw) {
        this.binaryIncomeWithdraw = binaryIncomeWithdraw;
    }

    public Integer getBinaryIncomeBalance() {
        return binaryIncomeBalance;
    }

    public void setBinaryIncomeBalance(Integer binaryIncomeBalance) {
        this.binaryIncomeBalance = binaryIncomeBalance;
    }

    public Integer getTopUpWallet() {
        return topUpWallet;
    }

    public void setTopUpWallet(Integer topUpWallet) {
        this.topUpWallet = topUpWallet;
    }

    public Integer getTopUpWalletWithdraw() {
        return topUpWalletWithdraw;
    }

    public void setTopUpWalletWithdraw(Integer topUpWalletWithdraw) {
        this.topUpWalletWithdraw = topUpWalletWithdraw;
    }

    public Integer getTopUpWalletBalance() {
        return topUpWalletBalance;
    }

    public void setTopUpWalletBalance(Integer topUpWalletBalance) {
        this.topUpWalletBalance = topUpWalletBalance;
    }

    public Integer getTransferWallet() {
        return transferWallet;
    }

    public void setTransferWallet(Integer transferWallet) {
        this.transferWallet = transferWallet;
    }

    public Integer getTransferWalletWithdraw() {
        return transferWalletWithdraw;
    }

    public void setTransferWalletWithdraw(Integer transferWalletWithdraw) {
        this.transferWalletWithdraw = transferWalletWithdraw;
    }

    public Integer getTransferWalletBalance() {
        return transferWalletBalance;
    }

    public void setTransferWalletBalance(Integer transferWalletBalance) {
        this.transferWalletBalance = transferWalletBalance;
    }

    public Integer getWorkingWallet() {
        return workingWallet;
    }

    public void setWorkingWallet(Integer workingWallet) {
        this.workingWallet = workingWallet;
    }

    public Integer getWorkingWalletWithdraw() {
        return workingWalletWithdraw;
    }

    public void setWorkingWalletWithdraw(Integer workingWalletWithdraw) {
        this.workingWalletWithdraw = workingWalletWithdraw;
    }

    public Integer getWorkingWalletBalance() {
        return workingWalletBalance;
    }

    public void setWorkingWalletBalance(Integer workingWalletBalance) {
        this.workingWalletBalance = workingWalletBalance;
    }

    public Integer getSingleLegBalance() {
        return singleLegBalance;
    }

    public void setSingleLegBalance(Integer singleLegBalance) {
        this.singleLegBalance = singleLegBalance;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Integer getTotalPh() {
        return totalPh;
    }

    public void setTotalPh(Integer totalPh) {
        this.totalPh = totalPh;
    }

    public String getTotalGiveAmount() {
        return totalGiveAmount;
    }

    public void setTotalGiveAmount(String totalGiveAmount) {
        this.totalGiveAmount = totalGiveAmount;
    }

    public String getPendingPh() {
        return pendingPh;
    }

    public void setPendingPh(String pendingPh) {
        this.pendingPh = pendingPh;
    }

    public String getTotalGh() {
        return totalGh;
    }

    public void setTotalGh(String totalGh) {
        this.totalGh = totalGh;
    }

    public Integer getTotalGetAmount() {
        return totalGetAmount;
    }

    public void setTotalGetAmount(Integer totalGetAmount) {
        this.totalGetAmount = totalGetAmount;
    }

    public Integer getPendingGh() {
        return pendingGh;
    }

    public void setPendingGh(Integer pendingGh) {
        this.pendingGh = pendingGh;
    }

    public Integer getPhCount() {
        return phCount;
    }

    public void setPhCount(Integer phCount) {
        this.phCount = phCount;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getRefUrl() {
        return refUrl;
    }

    public void setRefUrl(String refUrl) {
        this.refUrl = refUrl;
    }

    public String getGhReleaseDate() {
        return ghReleaseDate;
    }

    public void setGhReleaseDate(String ghReleaseDate) {
        this.ghReleaseDate = ghReleaseDate;
    }

}
