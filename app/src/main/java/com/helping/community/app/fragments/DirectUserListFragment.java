package com.helping.community.app.fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;
import com.helping.community.app.R;
import com.helping.community.app.adapter.DirectUserListAdapter;
import com.helping.community.app.models.DirectUserListRecordsModel;
import com.helping.community.app.models.DirectUserListResponseModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.Utils;
import com.helping.community.app.utils.ViewUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class DirectUserListFragment extends Fragment {

    private ListView mListEPinDetails;
    private static int mIndex = 0;
    private DirectUserListAdapter adapter;
    private List<DirectUserListRecordsModel> mHistoryList = new ArrayList<>();

    public static DirectUserListFragment newInstance() {
        DirectUserListFragment fragment = new DirectUserListFragment();
        return fragment;
    }

    public DirectUserListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_direct_user_list, container, false);
        mIndex = 0;
        initializeViews(view);
        registerListeners();
        if (Utils.checkInternetConnection(DirectUserListFragment.this.getContext())) {
            getDirectUserList();
        } else {
            Toast.makeText(DirectUserListFragment.this.getContext(),
                    getString(R.string.no_internet_connection_message), Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    private void registerListeners() {
        mListEPinDetails.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && (mListEPinDetails.getLastVisiblePosition() - mListEPinDetails.getHeaderViewsCount() -
                        mListEPinDetails.getFooterViewsCount()) >= (adapter.getCount() - 1)) {

                    if (Utils.checkInternetConnection(DirectUserListFragment.this.getContext())) {
                        getDirectUserList();
                    } else {
                        Toast.makeText(DirectUserListFragment.this.getContext(), getString(R.string.no_internet_connection_message), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    private void getDirectUserList() {
        final ProgressDialog pd = ViewUtils.getProgressBar(DirectUserListFragment.this.getContext(),
                "Loading...", "Please wait..!");
        Map<String, String> loginMap = new HashMap<>();

        loginMap.put("start", String.valueOf(mIndex));
        loginMap.put("length", "10");

        HelpingServices apiService = ApiHandler.getApiService();
        final Call<DirectUserListResponseModel> loginCall = apiService.wsDirectUserListDrawer(
                "Bearer " + SharedPreferenceUtils.getLoginObject(
                        DirectUserListFragment.this.getContext()).getData().getAccess_token(),
                loginMap);
        loginCall.enqueue(new Callback<DirectUserListResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<DirectUserListResponseModel> call,
                                   Response<DirectUserListResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    DirectUserListResponseModel loginModel = response.body();
                    if (loginModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            loginModel.getStatus().equals("OK")) {
                        mHistoryList.addAll(loginModel.getData().getRecords());
                        adapter = new DirectUserListAdapter(DirectUserListFragment.this.getContext(), mHistoryList);
                        mListEPinDetails.setAdapter(adapter);
                        mListEPinDetails.setSelection(mIndex);
                        mIndex = mIndex + loginModel.getData().getRecords().size();
                    }
                }
            }

            @Override
            public void onFailure(Call<DirectUserListResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(DirectUserListFragment.this.getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initializeViews(View view) {
        mListEPinDetails = view.findViewById(R.id.list_epin_details);
    }


}
