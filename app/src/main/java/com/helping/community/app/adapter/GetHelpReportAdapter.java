package com.helping.community.app.adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.helping.community.app.R;
import com.helping.community.app.models.GetHelpReportRecordsModel;

import java.util.ArrayList;
import java.util.List;

public class GetHelpReportAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    List<GetHelpReportRecordsModel> mHistoryList;
    List<Integer> count;
    public GetHelpReportAdapter(Context context, List<GetHelpReportRecordsModel> list) {
        mContext = context;
        mHistoryList = new ArrayList<GetHelpReportRecordsModel>();
        mHistoryList.addAll(list);
        count = new ArrayList<>();

//        for (int i = 1; i <= mHistoryList.size(); i++) {

//            count.add(i);
//        }
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mHistoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.layout_get_help_report_list_item, null);
        }
        holder = new ViewHolder();
        final GetHelpReportRecordsModel details = mHistoryList.get(position);
        String getType = details.getLinkType();

            if(getType.equals("GET")) {
                count.add(position);
            }

        holder.text_sr_no = view.findViewById(R.id.text_sr_no);
        holder.text_status = view.findViewById(R.id.text_status);
        holder.text_entry_time = view.findViewById(R.id.text_entry_time);
        holder.assign_date = view.findViewById(R.id.assign_date);
        holder.text_confirm_date = view.findViewById(R.id.text_confirm_date);
        holder.text_tran_id = view.findViewById(R.id.text_tran_id);
        holder.text_commit_id = view.findViewById(R.id.text_commit_id);
        holder.text_link_amount = view.findViewById(R.id.text_link_amount);

        holder.srno = view.findViewById(R.id.srno);
        holder.status = view.findViewById(R.id.status);
        holder.entrytime = view.findViewById(R.id.entrytime);
        holder.assigndate = view.findViewById(R.id.assigndate);
        holder.confirmdate = view.findViewById(R.id.confirmdate);
        holder.tranid = view.findViewById(R.id.tranid);
        holder.commitid = view.findViewById(R.id.commitid);
        holder.linkamount = view.findViewById(R.id.linkamount);

        if(getType.equals("GET")) {
            for(int i = 1; i <= count.size(); i++){
                holder.text_sr_no.setText(Integer.toString(i));
            }

            holder.text_status.setText(details.getStatus().toString());
            holder.text_entry_time.setText(details.getEntryTime().toString());
            holder.assign_date.setText(details.getAssignDate().toString());
            holder.text_confirm_date.setText(details.getConfirmDate());
            holder.text_tran_id.setText(details.getTranid().toString());
            holder.text_commit_id.setText(details.getCommitId().toString());
            holder.text_link_amount.setText(details.getLinkAmount().toString());

        }else{
        //    Toast.makeText(mContext, "No Record Found", Toast.LENGTH_LONG).show();
        }
        if(getType.equals("PROVIDE")){
            holder.text_sr_no.setVisibility(View.GONE);
            holder.text_status.setVisibility(View.GONE);
            holder.text_entry_time.setVisibility(View.GONE);
            holder.assign_date.setVisibility(View.GONE);
            holder.text_confirm_date.setVisibility(View.GONE);
            holder.text_tran_id.setVisibility(View.GONE);
            holder.text_commit_id.setVisibility(View.GONE);
            holder.text_link_amount.setVisibility(View.GONE);

            holder.srno.setVisibility(View.GONE);
            holder.status.setVisibility(View.GONE);
            holder.entrytime.setVisibility(View.GONE);
            holder.assigndate.setVisibility(View.GONE);
            holder.confirmdate.setVisibility(View.GONE);
            holder.tranid.setVisibility(View.GONE);
            holder.commitid.setVisibility(View.GONE);
            holder.linkamount.setVisibility(View.GONE);
        }

        return view;
    }

    public static class ViewHolder {
        TextView text_sr_no, text_status, text_entry_time, assign_date, text_confirm_date, text_tran_id,text_commit_id,text_link_amount,
        srno,status,entrytime,assigndate,confirmdate,tranid,commitid,linkamount;
    }
}
