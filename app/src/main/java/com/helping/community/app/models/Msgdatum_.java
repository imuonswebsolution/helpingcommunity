package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Msgdatum_ {
    @SerializedName("left")
    @Expose
    private Left_ left;
    @SerializedName("right")
    @Expose
    private Right_ right;

    public Left_ getLeft() {
        return left;
    }

    public void setLeft(Left_ left) {
        this.left = left;
    }

    public Right_ getRight() {
        return right;
    }

    public void setRight(Right_ right) {
        this.right = right;
    }

}
