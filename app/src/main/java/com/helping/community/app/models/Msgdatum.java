package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Msgdatum {
    @SerializedName("left")
    @Expose
    private Left left;
    @SerializedName("right")
    @Expose
    private Right right;

    public Left getLeft() {
        return left;
    }

    public void setLeft(Left left) {
        this.left = left;
    }

    public Right getRight() {
        return right;
    }

    public void setRight(Right right) {
        this.right = right;
    }
}
