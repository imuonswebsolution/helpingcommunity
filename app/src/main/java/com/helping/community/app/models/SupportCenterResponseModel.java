package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SupportCenterResponseModel {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private SupportCenterDataModel supportCenterDataModel;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SupportCenterDataModel getSupportCenteDataModel() {
        return supportCenterDataModel;
    }

    public void setUserProfileDataModel(SupportCenterDataModel supportCenterDataModel) {
        this.supportCenterDataModel = supportCenterDataModel;
    }

}
