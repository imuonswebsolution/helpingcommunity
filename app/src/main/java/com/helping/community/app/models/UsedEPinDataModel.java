package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UsedEPinDataModel implements Serializable {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("user_id")
    @Expose
    public String user_id;
    @SerializedName("product_id")
    @Expose
    public String product_id;
    @SerializedName("pin")
    @Expose
    public String pin;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("product_name")
    @Expose
    public String product_name;
    @SerializedName("used_by")
    @Expose
    public String used_by;
    @SerializedName("used_by_userId")
    @Expose
    public String used_by_userId;
    @SerializedName("fullname")
    @Expose
    public String fullname;
    @SerializedName("used_date")
    @Expose
    public String used_date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getUsed_by() {
        return used_by;
    }

    public void setUsed_by(String used_by) {
        this.used_by = used_by;
    }

    public String getUsed_by_userId() {
        return used_by_userId;
    }

    public void setUsed_by_userId(String used_by_userId) {
        this.used_by_userId = used_by_userId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUsed_date() {
        return used_date;
    }

    public void setUsed_date(String used_date) {
        this.used_date = used_date;
    }
}
