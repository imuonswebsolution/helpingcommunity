package com.helping.community.app.retrofit;



import com.helping.community.app.models.ChatReponseModel;
import com.helping.community.app.models.ConfirmResponseModel;
import com.helping.community.app.models.DashboardResponseModel;
import com.helping.community.app.models.DirectIncomeReportResponseModel;
import com.helping.community.app.models.DirectUserListResponseModel;
import com.helping.community.app.models.EPinDetailsResponseModel;
import com.helping.community.app.models.ForgotPasswordResponseModel;
import com.helping.community.app.models.GetHelpReportResponseModel;
import com.helping.community.app.models.HelpDetailsModel;
import com.helping.community.app.models.LinkDetailsModel;
import com.helping.community.app.models.MakeWithdrawalResponseModel;
import com.helping.community.app.models.Message;
import com.helping.community.app.models.Msgdatum_;
import com.helping.community.app.models.PinTransferHistoryResponseModel;
import com.helping.community.app.models.ProvideHelpReportResponseModel;
import com.helping.community.app.models.LoginResponseModel;

import com.helping.community.app.models.ReceivedEPinResponseModel;
import com.helping.community.app.models.RegisterResponseModel;
import com.helping.community.app.models.RejectResponseModel;
import com.helping.community.app.models.ResponseModelImage;
import com.helping.community.app.models.SupportCenterResponseModel;
import com.helping.community.app.models.TransferPinDetailsResponseModel;
import com.helping.community.app.models.UsedEPinResponseModel;
import com.helping.community.app.models.UserProfileResponseModel;
import com.helping.community.app.models.ViewSlipResponseModel;
import com.helping.community.app.models.WithdrawalIncomeOTPResponseModel;
import com.helping.community.app.models.WithdrawalResponseModel;


import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;


public interface HelpingServices {

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponseModel> wsLogin(@FieldMap Map<String, String> map);


    @GET("user/profile")
    Call<UserProfileResponseModel> wsUserProfileInfo(@Header("Authorization") String authHeader);

    @FormUrlEncoded
    @POST("user/get_transfer_pin_report")
    Call<PinTransferHistoryResponseModel> wsTransferEpinHistory(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);

    @FormUrlEncoded
    @POST("user/getTransferPinDetailReport")
    Call<TransferPinDetailsResponseModel> wsGetTraansferPinDetails(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);

    @FormUrlEncoded
    @POST("user/link-report")
    Call<LinkDetailsModel> wsLinkReport(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);

    @FormUrlEncoded
    @POST("user/get_receive_pin_report")
    Call<ReceivedEPinResponseModel> wsGetReceivedEpin(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);

    @FormUrlEncoded
    @POST("register")
    Call<RegisterResponseModel> wsRegister(@FieldMap Map<String, String> map);


    @FormUrlEncoded
    @POST("user/withdraw-income-otp")
    Call<MakeWithdrawalResponseModel> wsMakeWithdrawal(@Header("Authorization") String authHeader, @FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("user/withdraw-income")
    Call<WithdrawalIncomeOTPResponseModel> wsWithdrawalIncomeOTP(@Header("Authorization") String authHeader, @FieldMap Map<String, String> map);



    @FormUrlEncoded
    @POST("user/withdraw-history")
    Call<WithdrawalResponseModel> wswithdrawal(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);
    @FormUrlEncoded
    @POST("forgot-password")
    Call<ForgotPasswordResponseModel> wsCheckUserExist(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("user/link-report")
    Call<ProvideHelpReportResponseModel> wsDirectUserList(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);

    @FormUrlEncoded
    @POST("user/link-report")
    Call<GetHelpReportResponseModel> wsGetHelpList(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);

    @FormUrlEncoded
    @POST("user/enquiry")
    Call<SupportCenterResponseModel> wsSupport(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);

    @FormUrlEncoded
    @POST("user/epin-details")
    Call<EPinDetailsResponseModel> wsEPinDetailsReport(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);

    @FormUrlEncoded
    @POST("user/epin-details")
    Call<EPinDetailsResponseModel> wsUnusedEPinReport(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);


    @FormUrlEncoded
    @POST("user/level-view")
    Call<DirectUserListResponseModel> wsDirectUserListDrawer(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);


    @FormUrlEncoded
    @POST("user/direct-income")
    Call<DirectIncomeReportResponseModel> wsDirectUserIncomeReport(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);



    @FormUrlEncoded
    @POST("user/inactive_epins")
    Call<UsedEPinResponseModel> wsUsedPinHistoryReport(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);

    @FormUrlEncoded
    @POST("user/pay-on-link")
    Call<HelpDetailsModel> wsLinkHelpDetails(@Header("Authorization") String authHeader,@FieldMap Map<String, String> loginMap);

    @FormUrlEncoded
    @POST("user/linkchat")
    Call<ChatReponseModel> wsLinkChat(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);

    @FormUrlEncoded
    @POST("user/savelinkchat")
    Call<ChatReponseModel> wsSaveLinkChat(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);


    @Multipart
    @POST("user/upload-transaction-slip")
    Call<ResponseModelImage> wsSendQuery(@Header("Authorization") String authHeader, @PartMap() Map<String, RequestBody> partMap
            ,   @Part MultipartBody.Part file
    ); //,   @Part MultipartBody.Part file

    @FormUrlEncoded
    @POST("user/transaction-slip")
    Call<ViewSlipResponseModel> wsViewSlip(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);



    @GET("user/dashboard")
    Call<DashboardResponseModel> wsGetDashboardData(@Header("Authorization") String authHeader);



    @FormUrlEncoded
    @POST("user/confirmlink")
    Call<ConfirmResponseModel> wsConfirm(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);

    @FormUrlEncoded
    @POST("user/rejectlink")
    Call<RejectResponseModel> wsReject(@Header("Authorization") String authHeader, @FieldMap Map<String, String> loginMap);
}
