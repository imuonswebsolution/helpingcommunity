package com.helping.community.app.views;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.helping.community.app.R;
import com.helping.community.app.utils.SharedPreferenceUtils;

public class SplashActivity extends AppCompatActivity {
    public final String KEY_USERNAME = "username";
    public final String KEY_PASSWORD = "password";

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }


//


        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    sleep(3 * 1000);
                    Intent intent = null;

                    if (SharedPreferenceUtils.getAccesstoken(SplashActivity.this) == null) {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    } else {
                        startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
                    }
                    finish();
                } catch (Exception e) {
                }
            }
        };

        // start thread
        background.start();
    }


}