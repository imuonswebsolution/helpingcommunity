package com.helping.community.app.adapter;

import android.annotation.SuppressLint;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.helping.community.app.R;

import com.helping.community.app.models.ConfirmResponseModel;
import com.helping.community.app.models.Record;
import com.helping.community.app.models.RejectResponseModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.ViewUtils;
import com.helping.community.app.views.Chat;
import com.helping.community.app.views.HelpDetailsActivity;
import com.helping.community.app.views.UploadSlip;
import com.helping.community.app.views.ViewSlip;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LinkdetailsListviewAdapter extends BaseAdapter   {




    private List<Record> linkDetailsModelList;
    private String mstrStatus, mstrLinkType;
    Context context;
    String transactionidtosend,amounttosend,tranidreject;
    int commitidtosend,commitidreject;
    LayoutInflater inflater;
    public Record linkDetailsModel;
    private String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final int GET_FROM_GALLERY = 3;

    public LinkdetailsListviewAdapter(Context context, List<Record> list)
    {
        this.context = context;
        this.linkDetailsModelList = new ArrayList<>();
        this.linkDetailsModelList.addAll(list);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        return linkDetailsModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.layout_link_deatils, null);
        }
        holder = new ViewHolder();
        linkDetailsModel = linkDetailsModelList.get(position);


        holder.cardview1 = view.findViewById(R.id.helpdetails_cardview);

        if(!linkDetailsModel.getStatus().equals("Open"))
        {
           holder.cardview1.setVisibility(View.VISIBLE);
        }

        else {
            holder.cardview1.setVisibility(View.GONE);
        }


        final String transid =  linkDetailsModel.getTranid();

        holder.mTextProvieHelp = (TextView) view.findViewById(R.id.text_provide_help);
        holder.mTextDate = (TextView) view.findViewById(R.id.text_date);
        holder.mTextfromName = (TextView) view.findViewById(R.id.text_sub_fromName);
        holder.mTexttoName = (TextView) view.findViewById(R.id.text_sub_Toname);
        holder.mTextFUserId = (TextView) view.findViewById(R.id.text_from_USERId);
        holder.mTextTUserId = (TextView) view.findViewById(R.id.text_to_userId);
        holder.mTextfromFullName = (TextView) view.findViewById(R.id.text_from_fullname);
        holder.mTexttoFullName = (TextView) view.findViewById(R.id.text_to_userfullname);
        holder.mTextTransactionId = (TextView) view.findViewById(R.id.text_link_amount);
        holder.mTextLinkAmount_01 = (TextView) view.findViewById(R.id.text_link_amount_1);
        holder.mimageViewStatus = (ImageView) view.findViewById(R.id.image_status_icon);
        holder.mButtonLinkdedConfirm = (Button) view.findViewById(R.id.button_link_confirm);
        holder.mButtonDetails = (Button) view.findViewById(R.id.button_get_details);
//        holder.mButtonChat = (Button) view.findViewById(R.id.button_chat);
        holder.mButtonUploadSlip = (Button) view.findViewById(R.id.button_upload_payslip);
        holder.mButtonViewSlip = (Button) view.findViewById(R.id.button_view_payslip);
        holder.mTextTimer = (TextView) view.findViewById(R.id.text_time_over);
        holder.mTextCommitId = (TextView) view.findViewById(R.id.text_commit_id);
       holder.mButtonReject =  view.findViewById(R.id.button_link_reject);

        holder.mButtonDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent= new Intent(context,HelpDetailsActivity.class);
                intent.putExtra("transaction_id",transid);
                v.getContext().startActivity(intent);
            }
        });

        holder.mButtonUploadSlip.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v)
            {
                Intent intent= new Intent(context,UploadSlip.class);
                intent.putExtra("transaction_id",transid);
                v.getContext().startActivity(intent);

            }
        });

//        holder.mButtonChat.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent= new Intent(context, Chat.class);
//                intent.putExtra("transaction_id",transid);
//                v.getContext().startActivity(intent);
//            }
//        });

        holder.mButtonLinkdedConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                  showDialogconfirm();
                holder.mButtonReject.setVisibility(View.INVISIBLE);

                  holder.mButtonLinkdedConfirm.setVisibility(View.INVISIBLE);
            }
        });

        holder.mButtonViewSlip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewslip= new Intent(context,ViewSlip.class);
                viewslip.putExtra("transaction_id",transid);
                v.getContext().startActivity(viewslip);
            }
        });

         holder.mButtonReject.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 showDialogreject();
                 holder.mButtonReject.setVisibility(View.INVISIBLE);

                 holder.mButtonLinkdedConfirm.setVisibility(View.INVISIBLE);
             }
         });

        holder.mTextProvieHelp.setText(linkDetailsModel.getLinkType() + " HELP");
        holder.mTextDate.setText(linkDetailsModel.getEntryTime().substring(0, 10));
        holder.mTextfromName.setText(linkDetailsModel.getFromFullname().substring(0, 1));
        if(linkDetailsModel.getToFullname()!= null) {
            holder.mTexttoName.setText(linkDetailsModel.getToFullname().substring(0, 1));
        }
        if(linkDetailsModel.getFromUser()!= null) {
            holder.mTextFUserId.setText(linkDetailsModel.getFromUser().toString());
        }
        if(linkDetailsModel.getToUser()!= null) {
            holder.mTextTUserId.setText(linkDetailsModel.getToUser().toString());
        }
        holder.mTextfromFullName.setText(linkDetailsModel.getFromFullname());




            holder.mTexttoFullName.setText(linkDetailsModel.getToFullname());

        holder.mTextTransactionId.setText("TranID"+" "+linkDetailsModel.getTranid().toString());
        holder.mTextLinkAmount_01.setText("$" + linkDetailsModel.getLinkAmount());
        holder.mTextCommitId.setText("#" + linkDetailsModel.getCommitId().toString());


        //  View slip visibility
        if(linkDetailsModel.getReceiptStatus() == 1){
            holder.mButtonViewSlip.setVisibility(View.VISIBLE);}
        else {
            holder.mButtonViewSlip.setVisibility(View.GONE);
        }

// Upload slip visibility
        if(linkDetailsModel.getReceiptStatus()== 0 && linkDetailsModel.
                getLinkType().equals("PROVIDE") && linkDetailsModel.getStatus().equals("Pending")){
            holder.mButtonUploadSlip.setVisibility(View.VISIBLE);}

        else {   holder.mButtonUploadSlip.setVisibility(View.GONE);}


        mstrStatus = linkDetailsModel.getStatus();
        mstrLinkType = linkDetailsModel.getLinkType();

        if (mstrStatus.equals("Pending")) {
            holder.mimageViewStatus.setImageResource(R.drawable.ic_running_status);

            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            try {
                Date assigndate = dateFormat.parse(String.valueOf(linkDetailsModel.getCurrentDate().getDate()));
                Date lastDate = dateFormat.parse(String.valueOf(linkDetailsModel.getLinktimeNAssigndate().getDate()));

                long diff = lastDate.getTime() - assigndate.getTime();
                long Hours = diff / (60 * 60 * 1000) % 24;
//                long Hours = diff / (1000 * 60 * 60);
                long diffMinutes = diff / (60 * 1000) % 60;
                System.out.printf(" %d hours", diff);


                if (diffMinutes  <= 0 && Hours <= 0) {

                    holder.mTextTimer.setVisibility(View.VISIBLE);
                    holder.mTextTimer.setText(R.string.time_over);

                    if(linkDetailsModel.getLinkType().equals("GET")) {
                        holder.mButtonLinkdedConfirm.setVisibility(View.VISIBLE);
                    }
//                   holder.mButtonChat.setVisibility(View.VISIBLE);


                    if(linkDetailsModel.getLinkType().equals("GET")&& linkDetailsModel.getStatus()
                            .equals("Pending") )
                    {
                        tranidreject = linkDetailsModel.getTranid();
                        commitidreject = linkDetailsModel.getCommitId();
                        holder.mButtonReject.setVisibility(View.VISIBLE);

                    }
                    else {
                        holder.mButtonReject.setVisibility(View.INVISIBLE);
                    }




                } else {
             // IF TIMER IS RUNNING
                    String mStrhours = String.valueOf(Hours+ " "+"Hours"+" "+diffMinutes);
                    holder.mTextTimer.setVisibility(View.VISIBLE);
                    holder.mTextTimer.setText(mStrhours + " "+"Min");
//                    holder.mButtonChat.setVisibility(View.VISIBLE);
                    if(linkDetailsModel.getLinkType().equals("GET")&& linkDetailsModel.getStatus()
                            .equals("Pending") && linkDetailsModel.getReceiptStatus() == 1)
                    {
                        holder.mButtonLinkdedConfirm.setVisibility(View.VISIBLE);
                        transactionidtosend = linkDetailsModel.getTranid();
                        amounttosend = linkDetailsModel.getLinkAmount();
                        commitidtosend = linkDetailsModel.getCommitId();
                    }
                    else {
                        holder.mButtonLinkdedConfirm.setVisibility(View.INVISIBLE);
                    }




                }



            } catch (ParseException e) {
                e.printStackTrace();
            }


            if (mstrLinkType.equals("GET")) {
//                holder.mButtonLinkdedConfirm.setVisibility(View.VISIBLE);
                holder.mButtonDetails.setVisibility(View.VISIBLE);
            }
        } else if (mstrStatus.equals("Confirm")) {
            holder.mimageViewStatus.setImageResource(R.drawable.ic_complete_status);
            holder.mTextTimer.setVisibility(View.GONE);
            if (mstrLinkType.equals("GET")) {
//                holder.mButtonLinkdedConfirm.setVisibility(View.VISIBLE);
                holder.mButtonDetails.setVisibility(View.VISIBLE);
            }
            else if(mstrLinkType.equals("PROVIDE")) {
//                holder.mButtonLinkdedConfirm.setVisibility(View.INVISIBLE);
                holder.mButtonDetails.setVisibility(View.VISIBLE);

//                holder.mButtonDetails.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Gson gS = new Gson();
//                        String object = gS.toJson(linkDetailsModelList.get(position));
//                        mContext.startActivity(new Intent(mContext, SendHelpActivity.class)
//                                .putExtra("object", object));
//                    }
//                });
            }
        }

        return view;
    }

    private void showDialogreject() {
        AlertDialog.Builder builderreject = new AlertDialog.Builder(context);
        builderreject.setTitle("Exit");
        builderreject.setMessage("Are you sure you want to Reject ?");
        builderreject.setCancelable(true);

        builderreject.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        callrejectapi();
                        dialog.cancel();



                    }
                });

        builderreject.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertreject = builderreject.create();
        alertreject.show();
    }

    private void callrejectapi() {
        final ProgressDialog pd = ViewUtils.getProgressBar(context,
                "Loading...", "Please wait..!");
        Map<String, String> loginMap = new HashMap<>();


        loginMap.put("commit_id", String.valueOf(commitidreject));
        loginMap.put("transaction_id",tranidreject );

        HelpingServices apiService = ApiHandler.getApiService();
        final Call<RejectResponseModel> loginCall = apiService.wsReject(
                "Bearer " + SharedPreferenceUtils.getLoginObject(
                        context).getData().getAccess_token(),
                loginMap);
        loginCall.enqueue(new Callback<RejectResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<RejectResponseModel> call,
                                   Response<RejectResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    RejectResponseModel loginModel = response.body();
                    if (loginModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            loginModel.getStatus().equals("OK")) {
                        Toast.makeText(context, loginModel.getMessage(), Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(context, loginModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RejectResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showDialogconfirm() {
        AlertDialog.Builder builderconfirm = new AlertDialog.Builder(context);
        builderconfirm.setTitle("Exit");
        builderconfirm.setMessage("Are you sure you want to Confirm ?");
        builderconfirm.setCancelable(true);

        builderconfirm.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        callconfirmapi();
                        dialog.cancel();



                    }
                });

        builderconfirm.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertconfirm = builderconfirm.create();
        alertconfirm.show();
    }

    private void callconfirmapi() {
        final ProgressDialog pd = ViewUtils.getProgressBar(context,
                "Loading...", "Please wait..!");
        Map<String, String> loginMap = new HashMap<>();

        loginMap.put("amount",amounttosend);
        loginMap.put("commit_id", String.valueOf(commitidtosend));
        loginMap.put("transaction_id",transactionidtosend );

        HelpingServices apiService = ApiHandler.getApiService();
        final Call<ConfirmResponseModel> loginCall = apiService.wsConfirm(
                "Bearer " + SharedPreferenceUtils.getLoginObject(
                        context).getData().getAccess_token(),
                loginMap);
        loginCall.enqueue(new Callback<ConfirmResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<ConfirmResponseModel> call,
                                   Response<ConfirmResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    ConfirmResponseModel loginModel = response.body();
                    if (loginModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            loginModel.getStatus().equals("OK")) {
                        Toast.makeText(context, loginModel.getMessage(), Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(context, loginModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ConfirmResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public static class ViewHolder
    {
        TextView mTextProvieHelp, mTextDate, mTextfromName, mTexttoName,
                mTextFUserId, mTextTUserId, mTextfromFullName, mTexttoFullName, mTextTransactionId, mTextLinkAmount_01,
                mTextTimer, mTextCommitId;
        ImageView mimageViewStatus;
        Button mButtonLinkdedConfirm, mButtonDetails,mButtonChat,mButtonUploadSlip,mButtonViewSlip,mButtonReject;
        RelativeLayout cardview1;
    }
}