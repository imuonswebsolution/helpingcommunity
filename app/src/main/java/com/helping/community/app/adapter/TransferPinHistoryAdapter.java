package com.helping.community.app.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.helping.community.app.R;
import com.helping.community.app.models.PinTransferHistoryRecordsModel;
import com.helping.community.app.models.TransferEpinDetailsDataModel;
import com.helping.community.app.models.TransferPinDetailsResponseModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.ViewUtils;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransferPinHistoryAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater inflater;
    List<PinTransferHistoryRecordsModel> mHistoryList;

    public TransferPinHistoryAdapter(Context context, List<PinTransferHistoryRecordsModel> list) {
        mContext = context;
        mHistoryList = new ArrayList<PinTransferHistoryRecordsModel>();
        mHistoryList.addAll(list);
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mHistoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.layout_transfer_pin_history_list_item, null);
        }
        holder = new ViewHolder();
        final PinTransferHistoryRecordsModel details = mHistoryList.get(position);

        holder.textEPinCount = view.findViewById(R.id.text_e_pin_count);
        holder.textFullName = view.findViewById(R.id.text_full_name);
        holder.textUserId = view.findViewById(R.id.text_to_user_id);
        holder.textEPins = view.findViewById(R.id.text_e_pins);
        holder.textProductNames = view.findViewById(R.id.text_product_name);
        holder.textDate = view.findViewById(R.id.text_date);

        holder.textEPinCount.setText(details.getNoofpin());
        holder.textFullName.setText(details.getFullname());
        holder.textUserId.setText(details.getUser_id());
        holder.textProductNames.setText(details.getName());
        holder.textDate.setText(details.getEntry_time().split(" ")[0]);

        holder.textEPins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPinDetails(details);
            }
        });
        return view;
    }

    private void getPinDetails(PinTransferHistoryRecordsModel details) {
        final ProgressDialog pd = ViewUtils.getProgressBar(mContext, "Loading...", "Please wait..!");

        Map<String, String> loginMap = new HashMap<>();

        loginMap.put("history_id", details.getHistory_id());
        loginMap.put("touser_id", details.getTo_user_id());

        HelpingServices apiService = ApiHandler.getApiService();
        final Call<TransferPinDetailsResponseModel> loginCall = apiService.wsGetTraansferPinDetails("Bearer " + SharedPreferenceUtils.getLoginObject(
                mContext).getData().getAccess_token(), loginMap);
        loginCall.enqueue(new Callback<TransferPinDetailsResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<TransferPinDetailsResponseModel> call,
                                   Response<TransferPinDetailsResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    TransferPinDetailsResponseModel loginModel = response.body();
                    if (loginModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            loginModel.getStatus().equals("OK")) {
                        showAlert(loginModel.getData());
                    } else {
                        Toast.makeText(mContext, loginModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<TransferPinDetailsResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(mContext, mContext.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showAlert(List<TransferEpinDetailsDataModel> data) {
        // custom dialog
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.layout_pin_transfer_details_layout);

        TextView fromUser = (TextView) dialog.findViewById(R.id.text_epin);
        ImageView imageClose = dialog.findViewById(R.id.image_close);

        for (int i = 0; i < data.size(); i++) {
            fromUser.append(data.get(i).getPin() + "\n");
        }
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        imageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public static class ViewHolder {
        TextView textUserId, textFullName, textEPinCount, textEPins, textProductNames, textDate;
    }
}
