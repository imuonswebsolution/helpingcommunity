package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WithdrawalRecordsModel {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("withdraw_id")
    @Expose
    private Long withdrawId;
    @SerializedName("withdraw_date")
    @Expose
    private String withdrawDate;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("complete_date")
    @Expose
    private String completeDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("req_no")
    @Expose
    private Integer reqNo;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("withdraw_type")
    @Expose
    private String withdrawType;
    @SerializedName("current_date")
    @Expose
    private CurrentDateWithdrawal currentDateWithdrawal;
    @SerializedName("gh_count")
    @Expose
    private Integer ghCount;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Long getWithdrawId() {
        return withdrawId;
    }

    public void setWithdrawId(Long withdrawId) {
        this.withdrawId = withdrawId;
    }

    public String getWithdrawDate() {
        return withdrawDate;
    }

    public void setWithdrawDate(String withdrawDate) {
        this.withdrawDate = withdrawDate;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(String completeDate) {
        this.completeDate = completeDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getReqNo() {
        return reqNo;
    }

    public void setReqNo(Integer reqNo) {
        this.reqNo = reqNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWithdrawType() {
        return withdrawType;
    }

    public void setWithdrawType(String withdrawType) {
        this.withdrawType = withdrawType;
    }

    public CurrentDateWithdrawal getCurrentDate() {
        return currentDateWithdrawal;
    }

    public void setCurrentDate(CurrentDateWithdrawal currentDateWithdrawal) {
        this.currentDateWithdrawal = currentDateWithdrawal;
    }

    public Integer getGhCount() {
        return ghCount;
    }

    public void setGhCount(Integer ghCount) {
        this.ghCount = ghCount;
    }
}
