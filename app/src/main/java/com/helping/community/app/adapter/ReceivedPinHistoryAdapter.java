package com.helping.community.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.helping.community.app.R;
import com.helping.community.app.models.ReceivedEPinRecordsModel;


import java.util.ArrayList;
import java.util.List;

public class ReceivedPinHistoryAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    List<ReceivedEPinRecordsModel> mHistoryList;

    public ReceivedPinHistoryAdapter(Context context, List<ReceivedEPinRecordsModel> list) {
        mContext = context;
        mHistoryList = new ArrayList<ReceivedEPinRecordsModel>();
        mHistoryList.addAll(list);
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mHistoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.layout_epin_details_list_item, null);
        }
        holder = new ViewHolder();
        final ReceivedEPinRecordsModel details = mHistoryList.get(position);

        holder.textFullName = view.findViewById(R.id.text_full_name);
        holder.textPinCount = view.findViewById(R.id.text_pin_count);
        holder.textUserId = view.findViewById(R.id.text_user_id);
        holder.textProductName = view.findViewById(R.id.text_product_name);

        holder.textFullName.setText(details.getFullname());
        holder.textPinCount.setText(details.getNoofpin());
        holder.textProductName.setText(details.getName());
        holder.textUserId.setText(details.getUser_id());

        return view;
    }

    public static class ViewHolder {
        TextView textUserId, textFullName, textPinCount, textProductName;
    }
}
