
package com.helping.community.app.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProvideHelpReportDataModel {

    @SerializedName("recordsTotal")
    @Expose
    private Integer recordsTotal;
    @SerializedName("recordsFiltered")
    @Expose
    private Integer recordsFiltered;
    @SerializedName("records")
    @Expose
    private List<ProvideHelpReportRecordsModel> provideHelpReportRecordsModels = null;
    @SerializedName("start")
    @Expose
    private Integer start;

    public Integer getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(Integer recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public Integer getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(Integer recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public List<ProvideHelpReportRecordsModel> getRecords() {
        return provideHelpReportRecordsModels;
    }

    public void setProvideHelpReportRecordsModels(List<ProvideHelpReportRecordsModel> provideHelpReportRecordsModels) {
        this.provideHelpReportRecordsModels = provideHelpReportRecordsModels;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

}
