
package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfileDataModel {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("sponser_id")
    @Expose
    private String sponserId;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("iso_code")
    @Expose
    private String isoCode;
    @SerializedName("btc_address")
    @Expose
    private Object btcAddress;
    @SerializedName("eth_address")
    @Expose
    private Object ethAddress;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("branch_name")
    @Expose
    private String branchName;
    @SerializedName("holder_name")
    @Expose
    private String holderName;
    @SerializedName("ifsc_code")
    @Expose
    private String ifscCode;
    @SerializedName("account_no")
    @Expose
    private String accountNo;
    @SerializedName("state")
    @Expose
    private Object state;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("paytm_no")
    @Expose
    private Object paytmNo;
    @SerializedName("tez_no")
    @Expose
    private Object tezNo;
    @SerializedName("phonepe_no")
    @Expose
    private Object phonepeNo;
    @SerializedName("mobikwik_no")
    @Expose
    private Object mobikwikNo;
    @SerializedName("flag")
    @Expose
    private Object flag;
    @SerializedName("server_time")
    @Expose
    private String serverTime;
    @SerializedName("current_time")
    @Expose
    private String currentTime;
    @SerializedName("ip_address")
    @Expose
    private String ipAddress;
    @SerializedName("joining_date")
    @Expose
    private Object joiningDate;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getSponserId() {
        return sponserId;
    }

    public void setSponserId(String sponserId) {
        this.sponserId = sponserId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public Object getBtcAddress() {
        return btcAddress;
    }

    public void setBtcAddress(Object btcAddress) {
        this.btcAddress = btcAddress;
    }

    public Object getEthAddress() {
        return ethAddress;
    }

    public void setEthAddress(Object ethAddress) {
        this.ethAddress = ethAddress;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getPaytmNo() {
        return paytmNo;
    }

    public void setPaytmNo(Object paytmNo) {
        this.paytmNo = paytmNo;
    }

    public Object getTezNo() {
        return tezNo;
    }

    public void setTezNo(Object tezNo) {
        this.tezNo = tezNo;
    }

    public Object getPhonepeNo() {
        return phonepeNo;
    }

    public void setPhonepeNo(Object phonepeNo) {
        this.phonepeNo = phonepeNo;
    }

    public Object getMobikwikNo() {
        return mobikwikNo;
    }

    public void setMobikwikNo(Object mobikwikNo) {
        this.mobikwikNo = mobikwikNo;
    }

    public Object getFlag() {
        return flag;
    }

    public void setFlag(Object flag) {
        this.flag = flag;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Object getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(Object joiningDate) {
        this.joiningDate = joiningDate;
    }

}
