package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TransferEpinDetailsDataModel implements Serializable {
    @SerializedName("pin")
    @Expose
    public String pin;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("pins")
    @Expose
    public List<Object> pins = null;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Object> getPins() {
        return pins;
    }

    public void setPins(List<Object> pins) {
        this.pins = pins;
    }
}
