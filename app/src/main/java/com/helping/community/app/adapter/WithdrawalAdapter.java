package com.helping.community.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.helping.community.app.R;
import com.helping.community.app.models.WithdrawalRecordsModel;

import java.util.ArrayList;
import java.util.List;

public class WithdrawalAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    List<WithdrawalRecordsModel> mHistoryList;
    List<Integer> count;
    public WithdrawalAdapter(Context context, List<WithdrawalRecordsModel> list) {
        mContext = context;
        mHistoryList = new ArrayList<WithdrawalRecordsModel>();
        mHistoryList.addAll(list);
        count = new ArrayList<>();

        for (int i = 1; i <= mHistoryList.size(); i++) {
            count.add(i);
        }
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mHistoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.layout_withdrawal_list_item, null);
        }
        holder = new ViewHolder();
        final WithdrawalRecordsModel details = mHistoryList.get(position);

        holder.text_sr_no = view.findViewById(R.id.text_sr_no);
        holder.text_userid = view.findViewById(R.id.text_userid);
        holder.text_reqno = view.findViewById(R.id.text_reqno);
        holder.text_amount = view.findViewById(R.id.text_amount);
        holder.text_withdrawadate = view.findViewById(R.id.text_withdrawadate);
        holder.text_completedate = view.findViewById(R.id.text_completedate);
        holder.text_status = view.findViewById(R.id.text_status);


            holder.text_sr_no.setText(Integer.toString(count.get(position)));
            holder.text_userid.setText(details.getFullname());
            holder.text_reqno.setText(details.getReqNo().toString());
            holder.text_amount.setText(details.getAmount().toString());
            holder.text_withdrawadate.setText(details.getWithdrawDate().toString());

            if(details.getCompleteDate() != null){
                holder.text_completedate.setText(details.getCompleteDate());
            }else{
                holder.text_completedate.setText("");
            }



            holder.text_status.setText(details.getStatus().toString());



        return view;
    }

    public static class ViewHolder {
        TextView text_sr_no, text_userid, text_reqno, text_amount, text_withdrawadate, text_completedate,text_status;
    }
}
