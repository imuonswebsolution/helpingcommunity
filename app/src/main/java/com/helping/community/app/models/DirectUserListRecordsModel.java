
package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DirectUserListRecordsModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id_fullname")
    @Expose
    private String userIdFullname;
    @SerializedName("down_id")
    @Expose
    private Integer downId;
    @SerializedName("down_user_id_fullname")
    @Expose
    private String downUserIdFullname;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("down_user_id")
    @Expose
    private String downUserId;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("entry_time")
    @Expose
    private String entryTime;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("c_amount")
    @Expose
    private String cAmount;
    @SerializedName("give_amount")
    @Expose
    private String giveAmount;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("level")
    @Expose
    private Integer level;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserIdFullname() {
        return userIdFullname;
    }

    public void setUserIdFullname(String userIdFullname) {
        this.userIdFullname = userIdFullname;
    }

    public Integer getDownId() {
        return downId;
    }

    public void setDownId(Integer downId) {
        this.downId = downId;
    }

    public String getDownUserIdFullname() {
        return downUserIdFullname;
    }

    public void setDownUserIdFullname(String downUserIdFullname) {
        this.downUserIdFullname = downUserIdFullname;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDownUserId() {
        return downUserId;
    }

    public void setDownUserId(String downUserId) {
        this.downUserId = downUserId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(String entryTime) {
        this.entryTime = entryTime;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCAmount() {
        return cAmount;
    }

    public void setCAmount(String cAmount) {
        this.cAmount = cAmount;
    }

    public String getGiveAmount() {
        return giveAmount;
    }

    public void setGiveAmount(String giveAmount) {
        this.giveAmount = giveAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

}
