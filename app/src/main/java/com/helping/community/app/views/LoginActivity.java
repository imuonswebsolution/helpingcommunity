package com.helping.community.app.views;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.helping.community.app.R;
import com.helping.community.app.models.LoginResponseModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.ViewUtils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mEditUserName, mEditPassword;
    private TextView mTextForgotPassword;
    private Button mButtonSubmit, mButtonSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        initializeViews();
        registerListeners();
    }

    private void registerListeners() {
        mButtonSubmit.setOnClickListener(this);
        mTextForgotPassword.setOnClickListener(this);

    }

    private void initializeViews() {
        mEditUserName = findViewById(R.id.edit_user_id);
        mEditPassword = findViewById(R.id.edit_password);
        mTextForgotPassword = findViewById(R.id.text_forgot_password);
        mButtonSubmit = findViewById(R.id.button_login);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_forgot_password:
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                break;
//            case R.id.button_sign_up:
//                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
//                break;
            case R.id.button_login:
                if (validateUserName() && validatePassword()) {
                    login();
                }
                break;
        }
    }

    private void login() {
        final ProgressDialog pd = ViewUtils.getProgressBar(LoginActivity.this, "Loading...", "Please wait..!");

        Map<String, String> loginMap = new HashMap<>();
        final String userName, password;
        userName = mEditUserName.getText().toString().trim();
        password = mEditPassword.getText().toString().trim();

        loginMap.put("user_id", userName);
        loginMap.put("password", password);

        HelpingServices apiService = ApiHandler.getApiService();
        final Call<LoginResponseModel> loginCall = apiService.wsLogin(loginMap);
        loginCall.enqueue(new Callback<LoginResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<LoginResponseModel> call,
                                   Response<LoginResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    LoginResponseModel loginModel = response.body();
                    if (loginModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            loginModel.getStatus().equals("OK")) {
                        SharedPreferenceUtils.storeLoginObject(loginModel, LoginActivity.this);
                        SharedPreferenceUtils.storeUserName(LoginActivity.this, userName);
                        SharedPreferenceUtils.storePassword(LoginActivity.this, password);
                        SharedPreferenceUtils.storeAccessToken(LoginActivity.this,loginModel.getData().getAccess_token());
                        startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, loginModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(LoginActivity.this, "Check username or password", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(LoginActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validatePassword() {
            String password = mEditPassword.getText().toString();
            if (password.isEmpty()) {
                Toast.makeText(LoginActivity.this, getString(R.string.invalid_password),
                        Toast.LENGTH_SHORT).show();
                return false;
            }
            return true;
        }

        private boolean validateUserName() {
            String userName = mEditUserName.getText().toString();
            if (userName.isEmpty()) {
                Toast.makeText(LoginActivity.this, getString(R.string.invalid_user_id),
                        Toast.LENGTH_SHORT).show();
                return false;
            }
            return true;
    }
}
