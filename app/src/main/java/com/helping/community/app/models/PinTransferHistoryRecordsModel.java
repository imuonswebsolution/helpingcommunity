package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PinTransferHistoryRecordsModel implements Serializable {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("fullname")
    @Expose
    public String fullname;
    @SerializedName("user_id")
    @Expose
    public String user_id;
    @SerializedName("noofpin")
    @Expose
    public String noofpin;
    @SerializedName("to_user_id")
    @Expose
    public String to_user_id;
    @SerializedName("history_id")
    @Expose
    public String history_id;
    @SerializedName("entry_time")
    @Expose
    public String entry_time;
    @SerializedName("pins")
    @Expose
    public List<Object> pins = null;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNoofpin() {
        return noofpin;
    }

    public void setNoofpin(String noofpin) {
        this.noofpin = noofpin;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getHistory_id() {
        return history_id;
    }

    public void setHistory_id(String history_id) {
        this.history_id = history_id;
    }

    public String getEntry_time() {
        return entry_time;
    }

    public void setEntry_time(String entry_time) {
        this.entry_time = entry_time;
    }

    public List<Object> getPins() {
        return pins;
    }

    public void setPins(List<Object> pins) {
        this.pins = pins;
    }
}
