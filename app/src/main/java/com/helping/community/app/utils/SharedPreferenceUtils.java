package com.helping.community.app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.helping.community.app.models.DashboardResponseModel;
import com.helping.community.app.models.LoginResponseModel;


import static android.content.Context.MODE_PRIVATE;

public class SharedPreferenceUtils {
    static String PREFERENCE_NAME = "ProductPref";
    static String LOGIN_OBJECT = "login_object";
    static String DASHBOARD_OBJECT = "dashboard_object";
    static String USER_NAME = "user_name";
    static String USER_PPASSWORD = "user_password";
    static  String ACCESSTOKEN = "accesstoken";
    static String USER_ID = "userid";
    public static void storeLoginObject(LoginResponseModel model, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE).edit();
        Gson gson = new Gson();
        String json = gson.toJson(model);
        editor.putString(LOGIN_OBJECT, json);
        editor.apply();
    }

    public static LoginResponseModel getLoginObject(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(PREFERENCE_NAME, 0);
        Gson gson = new Gson();
        String json = prefs.getString(LOGIN_OBJECT, "");
        return gson.fromJson(json, LoginResponseModel.class);
    }

    public static void storeDashboardObject(DashboardResponseModel model, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE).edit();
        Gson gson = new Gson();
        String json = gson.toJson(model);
        editor.putString(DASHBOARD_OBJECT, json);
        editor.apply();
    }

    public static DashboardResponseModel getDashboardObject(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(PREFERENCE_NAME, 0);
        Gson gson = new Gson();
        String json = prefs.getString(DASHBOARD_OBJECT, "");
        return gson.fromJson(json, DashboardResponseModel.class);
    }

    public static void storeUserName(Context context, String userId) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE).edit();
        editor.putString(USER_NAME, userId);
        editor.commit();
    }
    public static void storeUserId(Context context, String userId) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE).edit();
        editor.putString(USER_ID, userId);
        editor.commit();
    }
    public static String getUserId(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(PREFERENCE_NAME, 0);
        return prefs.getString(USER_ID, null);
    }
    public static String getUserName(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(PREFERENCE_NAME, 0);
        return prefs.getString(USER_NAME, null);
    }

    public static void storePassword(Context context, String password) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE).edit();
        editor.putString(USER_PPASSWORD, password);
        editor.commit();
    }
    public static void storeAccessToken(Context context, String accesstoken) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE).edit();
        editor.putString(ACCESSTOKEN, accesstoken);
        editor.commit();
    }
    public static String getAccesstoken(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(PREFERENCE_NAME, 0);
        return prefs.getString(ACCESSTOKEN, null);
    }

    public static String getPassword(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(PREFERENCE_NAME, 0);
        return prefs.getString(USER_PPASSWORD, null);
    }


//    public static void setAccessToken(@NonNull Context context, String token) {
//        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString("ACCESSTOKEN", token);
//        editor.apply();
//    }
//
//    public static String getAccessToken(@NonNull Context context) {
//        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
//        return sharedPreferences.getString("ACCESSTOKEN", null);
//    }


    public static void clearPreferences(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_NAME, 0);
        preferences.edit().remove(LOGIN_OBJECT).apply();

    }
    public static void clearID(Context context) {
        SharedPreferences preferencess = context.getSharedPreferences(PREFERENCE_NAME, 0);
       preferencess.edit().remove(USER_ID).apply();

    }

}
