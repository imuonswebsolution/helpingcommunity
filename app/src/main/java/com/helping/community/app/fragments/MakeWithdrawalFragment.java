package com.helping.community.app.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.helping.community.app.R;
import com.helping.community.app.models.DashboardDataModel;
import com.helping.community.app.models.DashboardResponseModel;
import com.helping.community.app.models.MakeWithdrawalResponseModel;
import com.helping.community.app.models.RegisterResponseModel;
import com.helping.community.app.models.SupportCenterResponseModel;
import com.helping.community.app.models.UserProfileDataModel;
import com.helping.community.app.models.UserProfileResponseModel;
import com.helping.community.app.models.WithdrawalIncomeOTPResponseModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.ViewUtils;
import com.helping.community.app.views.RegistrationActivity;
import com.helping.community.app.views.ThankYou;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MakeWithdrawalFragment extends Fragment implements View.OnClickListener {

    private TextView mbalance;
    private EditText edit_text_amount,mEditTextOtp;
    private Button mSubmitButton,mGenerateOtp;
    private LinearLayout layout;
   String binary_income_balance,direct_income_balance,level_income_balance,roi_balance,
           single_leg,topup_wallet,transfer_wallet,walletbalance,otp;
    TextInputLayout til;
    int mAmountTransfer, mAvailablebalance;



    public MakeWithdrawalFragment() {
        // Required empty public constructor
    }


    public static MakeWithdrawalFragment newInstance() {
        MakeWithdrawalFragment fragment = new MakeWithdrawalFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_make_withdrawal, container, false);
        initializeViews(view);

        getDashboardData();

        mGenerateOtp.setOnClickListener(this);
        mSubmitButton.setOnClickListener(this);

        return view;
    }


    private void registerListener() {
        final ProgressDialog pd = ViewUtils.getProgressBar(MakeWithdrawalFragment.this.getContext(),
                "Loading...", "Please wait..!");
        Map<String, String> loginMap = new HashMap<>();

        loginMap.put("working_wallet", edit_text_amount.getText().toString().trim());
        loginMap.put("working_Wallet_balance", mbalance.getText().toString().trim());
        loginMap.put("binary_income_balance", binary_income_balance);
        loginMap.put("direct_income_balance", direct_income_balance);
        loginMap.put("level_income_balance", level_income_balance);
        loginMap.put("roi_balance", roi_balance);
        loginMap.put("single_leg", single_leg);
        loginMap.put("topup_wallet", topup_wallet);
        loginMap.put("transfer_wallet", transfer_wallet);



        HelpingServices apiService = ApiHandler.getApiService();
        final Call<MakeWithdrawalResponseModel> loginCall = apiService.wsMakeWithdrawal( "Bearer " + SharedPreferenceUtils.getLoginObject(
                MakeWithdrawalFragment.this.getContext()).getData().getAccess_token(),loginMap);
        loginCall.enqueue(new Callback<MakeWithdrawalResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<MakeWithdrawalResponseModel> call,
                                   Response<MakeWithdrawalResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    MakeWithdrawalResponseModel MakeWithdrawalResponseModel = response.body();
                    if (!MakeWithdrawalResponseModel.getStatus().isEmpty() &&
                            MakeWithdrawalResponseModel.getStatus().trim().equalsIgnoreCase("OK")) {

                        Toast.makeText(MakeWithdrawalFragment.this.getContext(), "OTP Sent",
                                Toast.LENGTH_SHORT).show();
                        edit_text_amount.setEnabled(false);
//                        edit_text_amount.getText().clear();;

                    } else {
                        Toast.makeText(MakeWithdrawalFragment.this.getContext(), MakeWithdrawalResponseModel.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<MakeWithdrawalResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Log.e("ERROR", t.toString());
                Toast.makeText(MakeWithdrawalFragment.this.getContext(), "Something went wrong..Please try again..!",
                        Toast.LENGTH_SHORT).show();

            }
        });
    }
    private void registerListener2() {
        final ProgressDialog pd = ViewUtils.getProgressBar(MakeWithdrawalFragment.this.getContext(),
                "Loading...", "Please wait..!");
        Map<String, String> loginMap = new HashMap<>();

        loginMap.put("working_wallet", edit_text_amount.getText().toString().trim());
        loginMap.put("working_Wallet_balance", mbalance.getText().toString().trim());
        loginMap.put("binary_income_balance", binary_income_balance);
        loginMap.put("direct_income_balance", direct_income_balance);
        loginMap.put("level_income_balance", level_income_balance);
        loginMap.put("roi_balance", roi_balance);
        loginMap.put("single_leg", single_leg);
        loginMap.put("topup_wallet", topup_wallet);
        loginMap.put("transfer_wallet", transfer_wallet);
        loginMap.put("otp",  mEditTextOtp.getText().toString().trim());


        HelpingServices apiService = ApiHandler.getApiService();
        final Call<WithdrawalIncomeOTPResponseModel> loginCall = apiService.wsWithdrawalIncomeOTP( "Bearer " + SharedPreferenceUtils.getLoginObject(
                MakeWithdrawalFragment.this.getContext()).getData().getAccess_token(),loginMap);
        loginCall.enqueue(new Callback<WithdrawalIncomeOTPResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<WithdrawalIncomeOTPResponseModel> call,
                                   Response<WithdrawalIncomeOTPResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    WithdrawalIncomeOTPResponseModel MakeWithdrawalResponseModel = response.body();
                    if (!MakeWithdrawalResponseModel.getStatus().isEmpty() &&
                            MakeWithdrawalResponseModel.getStatus().trim().equalsIgnoreCase("OK")) {
//
                        Toast.makeText(MakeWithdrawalFragment.this.getContext(), "Amount Withdrawal Successfully.",
                                Toast.LENGTH_SHORT).show();
                        mGenerateOtp.setVisibility(View.VISIBLE);
                        mSubmitButton.setVisibility(View.GONE);
                        edit_text_amount.getText().clear();;
                        mEditTextOtp.getText().clear();;
                        layout.setVisibility(View.GONE);

                    } else {
                        Toast.makeText(MakeWithdrawalFragment.this.getContext(), MakeWithdrawalResponseModel.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<WithdrawalIncomeOTPResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Log.e("ERROR", t.toString());
                Toast.makeText(MakeWithdrawalFragment.this.getContext(), "Something went wrong..Please try again..!",
                        Toast.LENGTH_SHORT).show();

            }
        });
    }



    private void initializeViews(View view) {

        mbalance = view.findViewById(R.id.text_balance);
        edit_text_amount = view.findViewById(R.id.edit_text_amount);
        mSubmitButton = view.findViewById(R.id.button_submit);
      til = view.findViewById(R.id.text_input_layout);
      layout = view.findViewById(R.id.otp_visibility);
        mEditTextOtp = view.findViewById(R.id.edit_text_otp);
        mGenerateOtp = view.findViewById(R.id.button_generate_otp);



    }


    private void getDashboardData() {
        final ProgressDialog pd = ViewUtils.getProgressBar(MakeWithdrawalFragment.this.getContext(), "Loading...", "Please wait..!");
        Map<String, String> loginMap = new HashMap<>();

        HelpingServices apiService = ApiHandler.getApiService();
        final Call<DashboardResponseModel> loginCall = apiService.wsGetDashboardData("Bearer " + SharedPreferenceUtils.getAccesstoken(MakeWithdrawalFragment.this.getContext()) ); //SharedPreferenceUtils.getLoginObject(DashboardFragment.this.getContext()).getData().getAccess_token()
        loginCall.enqueue(new Callback<DashboardResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<DashboardResponseModel> call,
                                   Response<DashboardResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    DashboardResponseModel loginModel = response.body();

                    walletbalance = loginModel.getDashboardDataModel().getWorkingWalletBalance().toString();
                    binary_income_balance =loginModel.getDashboardDataModel().getBinaryIncomeBalance().toString();
                    direct_income_balance=loginModel.getDashboardDataModel().getDirectIncomeBalance().toString();

                    level_income_balance=loginModel.getDashboardDataModel().getLevelIncomeBalance().toString();
                    roi_balance=loginModel.getDashboardDataModel().getRoiIncomeBalance().toString();
                    single_leg=loginModel.getDashboardDataModel().getSingleLegBalance().toString();
                    topup_wallet=loginModel.getDashboardDataModel().getTopUpWalletBalance().toString();
                    transfer_wallet=loginModel.getDashboardDataModel().getTransferWalletBalance().toString();

                    mbalance.setText(walletbalance);

                    if (loginModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            loginModel.getStatus().equals("OK")) {

                    } else {
                        Toast.makeText(MakeWithdrawalFragment.this.getContext(), loginModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<DashboardResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(MakeWithdrawalFragment.this.getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.button_generate_otp:

                mAmountTransfer = Integer.parseInt(edit_text_amount.getText().toString().trim());
                mAvailablebalance = Integer.parseInt(walletbalance);
                if (mAvailablebalance >= mAmountTransfer && (mAmountTransfer % 8000) == 0 && mAmountTransfer >= 8000) {

//                    til.setVisibility(View.GONE);
                    til.setError("");
                    registerListener();
                    layout.setVisibility(View.VISIBLE);
                    mGenerateOtp.setVisibility(View.GONE);
                    mSubmitButton.setVisibility(View.VISIBLE);



//                            if (!otp.matches("")) {
//                                registerListener2();
//                                getDashboardData();
//                            }


                } else {
//                    til.setVisibility(View.VISIBLE);
                    til.setError("Amount should be 8000 \n or more and multiple of 8");

                    if (mAvailablebalance <= mAmountTransfer) {
                        registerListener();
                    }

                  break;

                }

            case R.id.button_submit:


                otp = mEditTextOtp.getText().toString().trim();

                if (!otp.matches("")) {
                    registerListener2();
                    getDashboardData();


                }
                break;

        }



    }
}
