package com.helping.community.app.views;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.helping.community.app.R;
import com.helping.community.app.adapter.MessageListAdapter;

import com.helping.community.app.models.ChatReponseModel;

import com.helping.community.app.models.Msgdatum;


import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;

import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.ViewUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Chat extends AppCompatActivity {
    String  transid;
    MessageListAdapter messageListAdapter;
    RecyclerView recyclerView;
    private List<Msgdatum> mHistoryList = new ArrayList<>() ;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity_test);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        recyclerView =findViewById(R.id.reyclerview_message_list);

        //set the back arrow in the toolbar
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setTitle("Chat");
//        }


        Intent intent = getIntent();
        transid = intent.getStringExtra("transaction_id");
        registerListener();
    }

    private void registerListener() {
        final ProgressDialog pd = ViewUtils.getProgressBar(Chat.this,
                "Loading...", "Please wait..!");
        Map<String, String> loginMap = new HashMap<>();


        loginMap.put("transaction_id", transid);

        HelpingServices apiService = ApiHandler.getApiService();
        final Call<ChatReponseModel> loginCall = apiService.wsLinkChat(
                "Bearer " + SharedPreferenceUtils.getLoginObject(
                        Chat.this).getData().getAccess_token(),
                loginMap);
        loginCall.enqueue(new Callback<ChatReponseModel >() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<ChatReponseModel > call,
                                   Response<ChatReponseModel > response) {
                pd.hide();
                if (response.isSuccessful()) {
                    ChatReponseModel loginModel = response.body();
                    if (loginModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            loginModel.getStatus().equals("OK")) {


                     mHistoryList.addAll(loginModel.getChatRecordModel().getZero().getMsgdata());
                        messageListAdapter = new MessageListAdapter(Chat.this, mHistoryList);
                        recyclerView.setAdapter(messageListAdapter);

                    }
                }
            }

            @Override
            public void onFailure(Call<ChatReponseModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(Chat.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });






    }
}
