package com.helping.community.app.fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;


import com.helping.community.app.R;
import com.helping.community.app.adapter.ReceivedPinHistoryAdapter;
import com.helping.community.app.models.ReceivedEPinRecordsModel;
import com.helping.community.app.models.ReceivedEPinResponseModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.Utils;
import com.helping.community.app.utils.ViewUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReceivedEPinReportFragment extends Fragment {

    private EditText mEditEPin;
    private ImageView mImageSearch;
    private ListView mListReceivedPinHistory;
    private static int mIndex = 0;
    private ReceivedPinHistoryAdapter adapter;
    private List<ReceivedEPinRecordsModel> mHistoryList = new ArrayList<>();
    private String mStringUserId = "";

    public static ReceivedEPinReportFragment newInstance() {
        ReceivedEPinReportFragment fragment = new ReceivedEPinReportFragment();
        return fragment;
    }

    public ReceivedEPinReportFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_received_epin_report, container, false);
        mIndex = 0;
        initializeViews(view);
        registerListeners();
        if (Utils.checkInternetConnection(ReceivedEPinReportFragment.this.getContext())) {
            getHistory(mStringUserId);
        } else {
            Toast.makeText(ReceivedEPinReportFragment.this.getContext(),
                    getString(R.string.no_internet_connection_message), Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    private void registerListeners() {
        mImageSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIndex = 0;
                mHistoryList.clear();
                if (validateUserId()) {
                    mStringUserId = mEditEPin.getText().toString().trim();
                    getHistory(mStringUserId);
                } else {
                    mStringUserId = "";
                    getHistory(mStringUserId);
                }
            }
        });

        mListReceivedPinHistory.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && (mListReceivedPinHistory.getLastVisiblePosition() - mListReceivedPinHistory.getHeaderViewsCount() -
                        mListReceivedPinHistory.getFooterViewsCount()) >= (adapter.getCount() - 1)) {

                    if (Utils.checkInternetConnection(ReceivedEPinReportFragment.this.getContext())) {
                        getHistory(mStringUserId);
                    } else {
                        Toast.makeText(ReceivedEPinReportFragment.this.getContext(), getString(R.string.no_internet_connection_message), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    private void getHistory(String userId) {
        final ProgressDialog pd = ViewUtils.getProgressBar(ReceivedEPinReportFragment.this.getContext(),
                "Loading...", "Please wait..!");
        Map<String, String> loginMap = new HashMap<>();

        loginMap.put("start", String.valueOf(mIndex));
        loginMap.put("length", "10");
        loginMap.put("user_id", userId);

        HelpingServices apiService = ApiHandler.getApiService();
        final Call<ReceivedEPinResponseModel> loginCall = apiService.wsGetReceivedEpin(
                "Bearer " + SharedPreferenceUtils.getLoginObject(
                        ReceivedEPinReportFragment.this.getContext()).getData().getAccess_token(),
                loginMap);
        loginCall.enqueue(new Callback<ReceivedEPinResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<ReceivedEPinResponseModel> call,
                                   Response<ReceivedEPinResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    ReceivedEPinResponseModel loginModel = response.body();
                    if (loginModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            loginModel.getStatus().equals("OK")) {
                        mHistoryList.addAll(loginModel.getData().getRecords());
                        adapter = new ReceivedPinHistoryAdapter(ReceivedEPinReportFragment.this.getContext(), mHistoryList);
                        mListReceivedPinHistory.setAdapter(adapter);
                        mListReceivedPinHistory.setSelection(mIndex);
                        mIndex = mIndex + loginModel.getData().getRecords().size();
                    }
                }
            }

            @Override
            public void onFailure(Call<ReceivedEPinResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(ReceivedEPinReportFragment.this.getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validateUserId() {
        String userId = mEditEPin.getText().toString().trim();
        if (userId.isEmpty()) {
            return false;
        }
        return true;
    }

    private void initializeViews(View view) {
        mEditEPin = view.findViewById(R.id.edit_epin);
        mImageSearch = view.findViewById(R.id.image_search);
        mListReceivedPinHistory = view.findViewById(R.id.list_received_pin_report);
    }

}
