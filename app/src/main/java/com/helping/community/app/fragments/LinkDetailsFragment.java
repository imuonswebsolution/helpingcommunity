package com.helping.community.app.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.helping.community.app.R;
import com.helping.community.app.adapter.LinkdetailsListviewAdapter;
import com.helping.community.app.models.LinkDetailsModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.Utils;
import com.helping.community.app.utils.ViewUtils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LinkDetailsFragment extends Fragment  {

    //RecyclerView mRecycleLinkDetails;
    ListView mListView;
    //LinkDetailsAdapter linkDetailsAdapter;
    LinkdetailsListviewAdapter linkdetailsListviewAdapter;
    private static int mIndex = 0;
    LinearLayoutManager mLayoutManager;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;


    public LinkDetailsFragment() {
        // Required empty public constructor
    }

    public static LinkDetailsFragment newInstance() {
        LinkDetailsFragment fragment = new LinkDetailsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mIndex = 0;
        View view = inflater.inflate(R.layout.fragment_link_details, container, false);

        mLayoutManager = new LinearLayoutManager(LinkDetailsFragment.this.getContext());

        initializeViews(view);

        if (Utils.checkInternetConnection(LinkDetailsFragment.this.getContext())) {
            getLinkDetails();
        } else {
            Toast.makeText(LinkDetailsFragment.this.getContext(),
                    getString(R.string.no_internet_connection_message), Toast.LENGTH_SHORT).show();
        }
        return view;
    }




    private void getLinkDetails() {
        final ProgressDialog pd = ViewUtils.getProgressBar
                (LinkDetailsFragment.this.getContext(), "Loading...", "Please wait..!");

        Map<String, String> loginMap = new HashMap<>();
        loginMap.put("open", "0");



//
        loginMap.put("start", "0");
//

        HelpingServices apiService = ApiHandler.getApiService();
        final Call<LinkDetailsModel> loginCall = apiService.wsLinkReport("Bearer " + SharedPreferenceUtils.getLoginObject(
                LinkDetailsFragment.this.getContext()).getData().getAccess_token(),loginMap);
        loginCall.enqueue(new Callback<LinkDetailsModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<LinkDetailsModel> call,
                                   Response<LinkDetailsModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    LinkDetailsModel responseModel = response.body();
                    if (responseModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            responseModel.getStatus().equals("OK")) {
//                        Toast.makeText(LinkDetailsFragment.this.getContext(), responseModel.getMessage(), Toast.LENGTH_SHORT).show();

                       /* linkDetailsAdapter = new LinkDetailsAdapter(LinkDetailsFragment.this.getContext(), responseModel.getLinkDetailsDataModel().getRecords());
                        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        mRecycleLinkDetails.setLayoutManager(mLayoutManager);
                        mRecycleLinkDetails.setAdapter(linkDetailsAdapter);*/
                        linkdetailsListviewAdapter = new LinkdetailsListviewAdapter(LinkDetailsFragment.this.getContext(), responseModel.getData().getRecords());

                        mListView.setAdapter(linkdetailsListviewAdapter);
                        mListView.setSelection(mIndex);
//                         mRecycleLinkDetails.scrollToPosition(mIndex);
                        mIndex = mIndex + responseModel.getData().getRecords().size();
                    } else {
//                        Toast.makeText(LinkDetailsFragment.this.getContext(), responseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LinkDetailsModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(LinkDetailsFragment.this.getContext(),"Respose Fail" , Toast.LENGTH_SHORT).show();

//                getString(R.string.something_went_wrong)
            }
        });
    }


    private void initializeViews(View view) {
        //   mRecycleLinkDetails = view.findViewById(R.id.recyc_link_details);
        mListView = view.findViewById(R.id.list);

    }


}
