package com.helping.community.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.helping.community.app.R;
import com.helping.community.app.models.DirectUserListRecordsModel;


import java.util.ArrayList;
import java.util.List;

public class DirectUserListAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    List<DirectUserListRecordsModel> mHistoryList;

    public DirectUserListAdapter(Context context, List<DirectUserListRecordsModel> list) {
        mContext = context;
        mHistoryList = new ArrayList<DirectUserListRecordsModel>();
        mHistoryList.addAll(list);
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mHistoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.layout_direct_user_list_item, null);
        }
        holder = new ViewHolder();
        final DirectUserListRecordsModel details = mHistoryList.get(position);


        holder.textFullName = view.findViewById(R.id.text_full_name);
        holder.textUserId = view.findViewById(R.id.text_user_id);
        holder.textDate = view.findViewById(R.id.text_date);
        holder.textstatus = view.findViewById(R.id.text_status);



        holder.textFullName.setText(details.getFullname());
        holder.textUserId.setText(details.getDownUserId());
        holder.textDate.setText(details.getEntryTime().substring(0,10));
        holder.textstatus.setText(details.getStatus());



        return view;
    }

    public static class ViewHolder {
        TextView textUserId,  textFullName,  textDate, textstatus;
    }
}
