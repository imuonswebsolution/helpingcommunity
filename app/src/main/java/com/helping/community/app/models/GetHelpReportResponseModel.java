package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetHelpReportResponseModel {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private GetHelpReportDataModel data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GetHelpReportDataModel getData() {
        return data;
    }

    public void setData(GetHelpReportDataModel getHelpReportDataModel) {
        this.data = getHelpReportDataModel;
    }
}
