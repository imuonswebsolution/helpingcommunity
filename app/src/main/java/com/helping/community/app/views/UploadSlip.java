package com.helping.community.app.views;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.helping.community.app.R;

import com.helping.community.app.fragments.LinkDetailsFragment;
import com.helping.community.app.models.ResponseModelImage;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_SHORT;


public class UploadSlip extends AppCompatActivity implements View.OnClickListener {
    Button btn_select,btn_submit;
String trans_ID ;
String m_selectedPath;
    private static final int SELECT_PICTURE = 100;
    private static final String TAG = "SelectImageActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_upload_payslip);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Select Image");
 trans_ID = getIntent().getStringExtra("transaction_id");
        btn_select = findViewById(R.id.btn_select);
        btn_select.setOnClickListener(this);
        btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setVisibility(View.GONE);
        btn_submit.setOnClickListener(this);
        handlePermission();
    }

    private void handlePermission() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //ask for permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    SELECT_PICTURE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case SELECT_PICTURE:
                for (int i = 0; i < permissions.length; i++) {
                    String permission = permissions[i];
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                        if (showRationale) {
                            //  Show your own message here
                            Toast.makeText(this,"image uploaded",
                                    Toast.LENGTH_SHORT).show();

                        } else {
                            showSettingsAlert();
                        }
                    }
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /* Choose an image from Gallery */
    void openImageChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);


    }

    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (resultCode == RESULT_OK) {
                    if (requestCode == SELECT_PICTURE) {
                        // Get the url from data
                        final Uri selectedImageUri = data.getData();
                        if (null != selectedImageUri) {
                            // Get the path from the Uri
                            String path = getPathFromURI(selectedImageUri);
                            m_selectedPath = path;

                            Log.i(TAG, "Image Path : " + path);
                            // Set the image in ImageView
                            findViewById(R.id.imgView).post(new Runnable() {
                                @Override
                                public void run() {
                                    ((ImageView) findViewById(R.id.imgView)).setImageURI(selectedImageUri);
                                   // imageButton.setText("Submit Image");
                                    btn_select.setVisibility(View.GONE);
                                    btn_submit.setVisibility(View.VISIBLE);

                                }
                            });

                        }
                    }
                }
            }
        }).start();

    }

    /* Get the real path from the URI */
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId() ){
            case R.id.btn_select :
            openImageChooser();
            break;

            case R.id.btn_submit :
            //    Toast.makeText(UploadSlip.this,"check",LENGTH_SHORT).show();
            sendQuery();
                break;
        }


    }






        private void sendQuery() {

            final ProgressDialog pd = new ProgressDialog(UploadSlip.this);
            pd.setTitle("Sending Query..");
            pd.setMessage("Please wait...!");
            pd.setCancelable(false);
            pd.show();

          RequestBody transaction_id = RequestBody.create(MediaType.parse("text/plain"),trans_ID);
        MultipartBody.Part body = null;
          try {
            File file = new File(m_selectedPath);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            body = MultipartBody.Part.createFormData("attachment", file.getName().replace(" ", "_"), requestFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("transaction_id",transaction_id);
            //map.put("attachment",body);


            HelpingServices apiService = ApiHandler.getApiService();
            Call<ResponseModelImage> call = apiService.wsSendQuery("Bearer " + SharedPreferenceUtils.getLoginObject(
                    UploadSlip.this).getData().getAccess_token(), map,body); //, body
            call.enqueue(new Callback<ResponseModelImage>() {
                @Override
                public void onResponse(Call<ResponseModelImage> call, Response<ResponseModelImage> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        // tasks available
                        ResponseModelImage responseState = response.body();
                        if (responseState.getCode() == Constants.RESPONSE_CODE_OK) {
                            Toast.makeText(UploadSlip.this,
                                    responseState.getMessage(), Toast.LENGTH_LONG).show();

                           Intent i = new Intent(UploadSlip.this,DashboardActivity.class);
                           startActivity(i);

//                            if (m_selectedPath != null && !m_selectedPath.isEmpty()) {
//                                m_selectedPath ;
//                            }
                        } else {

                            Log.v("Error code 400",response.errorBody().toString());
                            Toast.makeText(UploadSlip.this,
                                    responseState.getMessage(), LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(UploadSlip.this,
                                "not uploading", LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseModelImage> call, Throwable t) {
                    // something went completely south (like no internet connection)
                    Toast.makeText(UploadSlip.this,
                            t.toString(), LENGTH_SHORT).show();
                    pd.dismiss();
                }
            });
        }




    private void showSettingsAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        openAppSettings(UploadSlip.this);
                    }
                });
        alertDialog.show();
    }

    public static void openAppSettings(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

}