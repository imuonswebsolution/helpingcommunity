package com.helping.community.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.helping.community.app.R;
import com.helping.community.app.models.DirectIncomeReportRecordModel;
import com.helping.community.app.models.DirectUserListRecordsModel;

import java.util.ArrayList;
import java.util.List;

public class DirectUserIncomeAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    List<DirectIncomeReportRecordModel> mHistoryList;

    public DirectUserIncomeAdapter(Context context, List<DirectIncomeReportRecordModel> list) {
        mContext = context;
        mHistoryList = new ArrayList<DirectIncomeReportRecordModel>();
        mHistoryList.addAll(list);
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mHistoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.layout_direct_user_income_item, null);
        }
        holder = new ViewHolder();
        final DirectIncomeReportRecordModel details = mHistoryList.get(position);


        holder.textToUserid = view.findViewById(R.id.text_to_user_id);
        holder.textFromUserId = view.findViewById(R.id.text_from_user_id);
        holder.textDate = view.findViewById(R.id.text_date);
        holder.textAmount = view.findViewById(R.id.text_amount);



        holder.textToUserid.setText(details.getToUser().toString());
        holder.textFromUserId.setText(details.getFromUser().toString());
        holder.textDate.setText(details.getEntryTime().substring(0,10));
        holder.textAmount.setText(details.getAmount().toString());



        return view;
    }

    public static class ViewHolder {
        TextView textToUserid,  textFromUserId, textAmount, textDate;
    }
}
