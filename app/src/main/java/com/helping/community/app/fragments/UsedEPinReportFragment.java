package com.helping.community.app.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.helping.community.app.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class UsedEPinReportFragment extends Fragment {

    public static UsedEPinReportFragment newInstance() {
        UsedEPinReportFragment fragment = new UsedEPinReportFragment();
        return fragment;
    }

    public UsedEPinReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_used_epin_report, container, false);
    }

}
