package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

 public  class ChatRecordModel {
    @SerializedName("0")
    @Expose
    private Zero zero;
    @SerializedName("messages")
    @Expose
    private List<Message> messages = null;

    public Zero getZero() {
        return zero;
    }

    public void setZero(Zero zero) {
        this.zero = zero;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
