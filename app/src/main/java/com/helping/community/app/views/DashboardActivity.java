package com.helping.community.app.views;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.helping.community.app.R;
import com.helping.community.app.adapter.CustomExpandableListAdapter;
import com.helping.community.app.fragments.DashboardFragment;
import com.helping.community.app.fragments.DirectIcomeReportFragment;
import com.helping.community.app.fragments.DirectUserListFragment;
import com.helping.community.app.fragments.EPinDetailsFragment;
import com.helping.community.app.fragments.GetHelpReportFragment;
import com.helping.community.app.fragments.HelpDetailsFragment;
import com.helping.community.app.fragments.LinkDetailsFragment;
import com.helping.community.app.fragments.MakeWithdrawalFragment;
import com.helping.community.app.fragments.ProvideHelpReportFragment;
import com.helping.community.app.fragments.ReceivedEPinReportFragment;
import com.helping.community.app.fragments.SupportCenterFragment;
import com.helping.community.app.fragments.TransferPinHistoryFragment;
import com.helping.community.app.fragments.UnusedEpinReportFragment;
import com.helping.community.app.fragments.UsedEPinHistoryFragment;
import com.helping.community.app.fragments.UserProfileFragment;
import com.helping.community.app.fragments.WithdrawalFragment;
import com.helping.community.app.utils.SharedPreferenceUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DashboardActivity extends AppCompatActivity {

    private ExpandableListAdapter mExpandableListAdapter;
    private List<String> mExpandableListTitle;
    private Map<String, List<String>> mExpandableListData;
    Toolbar toolbar;
    ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ExpandableListView mExpandableListView;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private FragmentManager fragmentManager;
    TextView userid_drawer;
    private long lastPressedTime;
    private static final int PERIOD = 2000;
    View currentView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        userid_drawer = findViewById(R.id.userid_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.title_activity_dashboard);
        }


        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mExpandableListView = findViewById(R.id.navList);

        setupToolbar();

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);

        prepareListData();
        mExpandableListAdapter = new CustomExpandableListAdapter(DashboardActivity.this, mExpandableListTitle, mExpandableListData);
        mExpandableListView.setAdapter(mExpandableListAdapter);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        setupDrawerToggle();



        mExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                switch (groupPosition)
                {
                    case 0:
                        fragmentManager.beginTransaction().replace(R.id.content_frame, DashboardFragment.newInstance()).commit();
                        getSupportActionBar().setTitle("DASHBOARD");
                        mDrawerLayout.closeDrawers();
                        break;
                    case 1:
                        fragmentManager.beginTransaction().replace(R.id.content_frame, UserProfileFragment.newInstance()).commit();
                        getSupportActionBar().setTitle("PROFILE");
                        mDrawerLayout.closeDrawers();
                        break;
//                    case 3:
//                        fragmentManager.beginTransaction().replace(R.id.content_frame, WithdrawalFragment.newInstance()).commit();
//                        getSupportActionBar().setTitle("WITHDRAWAL");
//                        mDrawerLayout.closeDrawers();
//                        break;
                    case 5:
                    fragmentManager.beginTransaction().replace(R.id.content_frame, DirectUserListFragment.newInstance()).commit();
                    getSupportActionBar().setTitle("Direct Users List");
                    mDrawerLayout.closeDrawers();
                    break;

                    case 6:
                        fragmentManager.beginTransaction().replace(R.id.content_frame, DirectIcomeReportFragment.newInstance()).commit();
                        getSupportActionBar().setTitle("Direct Income Report");
                        mDrawerLayout.closeDrawers();
                        break;

                    case 7:
                        fragmentManager.beginTransaction().replace(R.id.content_frame, SupportCenterFragment.newInstance()).commit();
                        getSupportActionBar().setTitle("Support");
                        mDrawerLayout.closeDrawers();
                        break;
//                    case 8:
//                        fragmentManager.beginTransaction().replace(R.id.content_frame, BusinessPlanFragment.newInstance()).commit();
//                        getSupportActionBar().setTitle("Business Plan");
//                        mDrawerLayout.closeDrawers();
//                        break;

                    case 8:
                        showAlertDialog();
                        break;
                }
                return false;
            }
        });

//
        mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                switch (groupPosition) {

                    case 2:
                        switch (childPosition) {
                            case 0:
                                fragmentManager.beginTransaction().replace(R.id.content_frame, ProvideHelpReportFragment.newInstance()).commit();
                                getSupportActionBar().setTitle("Provide Help Report");
                                mExpandableListView.setItemChecked(childPosition, true);
                                mExpandableListView.setSelection(childPosition);
                                break;
                            case 1:
                                fragmentManager.beginTransaction().replace(R.id.content_frame, GetHelpReportFragment.newInstance()).commit();
                                getSupportActionBar().setTitle("Get Help Report");
                                mExpandableListView.setItemChecked(childPosition, true);
                                mExpandableListView.setSelection(childPosition);
                                break;
                            case 2:
                                fragmentManager.beginTransaction().replace(R.id.content_frame, LinkDetailsFragment.newInstance()).commit();
                                getSupportActionBar().setTitle("Link Details");
                                mExpandableListView.setItemChecked(childPosition, true);
                                mExpandableListView.setSelection(childPosition);
                                break;
//                            case 3:
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, HelpDetailsFragment.newInstance()).commit();
//                                getSupportActionBar().setTitle("Help Details View");
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                            case 2:
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, ProvideHelpReportFragment.newInstance()).commit();
//                                getSupportActionBar().setTitle("Direct User List");
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
                        }
                        break;

                    case 3:
                        switch (childPosition) {
                            case 0:
                                fragmentManager.beginTransaction().replace(R.id.content_frame, MakeWithdrawalFragment.newInstance()).commit();
                                getSupportActionBar().setTitle("Make Withdrawal");
                                mExpandableListView.setItemChecked(childPosition, true);
                                mExpandableListView.setSelection(childPosition);
                                break;
                            case 1:
                                fragmentManager.beginTransaction().replace(R.id.content_frame, WithdrawalFragment.newInstance()).commit();
                                getSupportActionBar().setTitle("Withdrawl History");
                                mExpandableListView.setItemChecked(childPosition, true);
                                mExpandableListView.setSelection(childPosition);
                                break;

                        }
                        break;



                    case 4:
                        switch (childPosition) {
                            case 0:
                                getSupportActionBar().setTitle("E-Pin Details");
                                fragmentManager.beginTransaction().replace(R.id.content_frame, EPinDetailsFragment.newInstance()).commit();
                                mExpandableListView.setItemChecked(childPosition, true);
                                mExpandableListView.setSelection(childPosition);
                                break;
                            case 1:
                                getSupportActionBar().setTitle("Used E-Pin Report");
                                fragmentManager.beginTransaction().replace(R.id.content_frame, UsedEPinHistoryFragment.newInstance()).commit();
                                mExpandableListView.setItemChecked(childPosition, true);
                                mExpandableListView.setSelection(childPosition);
                                break;
                            case 2:
                                getSupportActionBar().setTitle("Unused E-Pin");
                                fragmentManager.beginTransaction().replace(R.id.content_frame, UnusedEpinReportFragment.newInstance()).commit();
                                mExpandableListView.setItemChecked(childPosition, true);
                                mExpandableListView.setSelection(childPosition);
                                break;
                            case 3:
                                getSupportActionBar().setTitle("Transfer E-Pin Report");
                                fragmentManager.beginTransaction().replace(R.id.content_frame, TransferPinHistoryFragment.newInstance()).commit();
                                mExpandableListView.setItemChecked(childPosition, true);
                                mExpandableListView.setSelection(childPosition);
                                break;
                            case 4:
                                getSupportActionBar().setTitle("Received Pin Report");
                                fragmentManager.beginTransaction().replace(R.id.content_frame, ReceivedEPinReportFragment.newInstance()).commit();
                                mExpandableListView.setItemChecked(childPosition, true);
                                mExpandableListView.setSelection(childPosition);
                                break;
//                            case 2:
//                                getSupportActionBar().setTitle("Transfer E-Pin");
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, TransferPinFragment.newInstance()).commit();
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                            case 3:
//                                getSupportActionBar().setTitle("Transfer E-Pin Report");
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, TransferPinHistoryFragment.newInstance()).commit();
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                            case 4:
//                                getSupportActionBar().setTitle("Received Pin Report");
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, ReceivedEPinReportFragment.newInstance()).commit();
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                            case 5:
//                                getSupportActionBar().setTitle("E-Pin Request Report");
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, EPinRequestReportFragment.newInstance()).commit();
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
                        }
//                        break;
//                    case 4:
//                        switch (childPosition) {
//                            case 0:
//                                getSupportActionBar().setTitle("Distributor Income Report");
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, DistributorIncomeReportFragment.newInstance()).commit();
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                            case 1:
//                                getSupportActionBar().setTitle("Silver Income Report");
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, SilverIncomeReportFragment.newInstance()).commit();
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                            case 2:
//                                getSupportActionBar().setTitle("Gold Income Report");
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, GoldIncomeReportFragment.newInstance()).commit();
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                            case 3 :
//                                getSupportActionBar().setTitle("Platinum Income Report");
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, PlatinumIncomeReportFragment.newInstance()).commit();
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                            case 4:
//                                getSupportActionBar().setTitle("Diamond Income Report");
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, DiamondIncomeReportFragment.newInstance()).commit();
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                            case 5 :
//                                getSupportActionBar().setTitle("Repurchase Binary Income Report");
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, RepurchaseBinaryIncomeReportFragment.newInstance()).commit();
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                        }
//                        break;
//                    case 5:
//                        switch (childPosition) {
//                            case 0:
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, TopUpFragment.newInstance()).commit();
//                                getSupportActionBar().setTitle("Activate Account");
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                            case 1:
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, TopUpReportFragment.newInstance()).commit();
//                                getSupportActionBar().setTitle("Activate Account Report");
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                        }
//                        break;

//                    case 6:
//                        switch (childPosition) {
////                            case 0:
////                                getSupportActionBar().setTitle("Withdraw");
////                                fragmentManager.beginTransaction().replace(R.id.content_frame, WithdrawFragment.newInstance()).commit();
////                                mExpandableListView.setItemChecked(childPosition, true);
////                                mExpandableListView.setSelection(childPosition);
////                                break;
//                            case 0:
//                                getSupportActionBar().setTitle("Withdraw Pending");
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, WithdrawRequestFragment.newInstance()).commit();
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                            case 1:
//                                getSupportActionBar().setTitle("Withdraw Confirmed");
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, WithdrawHistoryFragment.newInstance()).commit();
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                        }
//                        break;

//                    case 8 :
//                        switch (childPosition) {
////                            case 0:
////                                getSupportActionBar().setTitle("Withdraw");
////                                fragmentManager.beginTransaction().replace(R.id.content_frame, WithdrawFragment.newInstance()).commit();
////                                mExpandableListView.setItemChecked(childPosition, true);
////                                mExpandableListView.setSelection(childPosition);
////                                break;
//                            case 0:
//                                getSupportActionBar().setTitle("Business Plan");
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, BusinessPlanFragment.newInstance()).commit();
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                            case 1:
//                                getSupportActionBar().setTitle("Repurchase Plan");
//                                fragmentManager.beginTransaction().replace(R.id.content_frame, RepurchasePlanFragment.newInstance()).commit();
//                                mExpandableListView.setItemChecked(childPosition, true);
//                                mExpandableListView.setSelection(childPosition);
//                                break;
//                        }
//                        break;
                }
                mDrawerLayout.closeDrawers();
                return false;

            }
        });

        mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                for (int i = 0; i < mExpandableListView.getCount(); i++) {
                    if (i != groupPosition) {
                        mExpandableListView.collapseGroup(i);
                    }
                }
            }
        });
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, DashboardFragment.newInstance()).commit();
        String userid =SharedPreferenceUtils.getUserId(DashboardActivity.this);
        userid_drawer.setText(userid);
    }


    void showAlertDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(DashboardActivity.this);
        builder1.setTitle("Exit");
        builder1.setMessage("Are you sure you want to Logout ?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        DashboardActivity.this.finish();
                        SharedPreferenceUtils.clearPreferences(DashboardActivity.this);
                        SharedPreferenceUtils.clearID(DashboardActivity.this);
                        startActivity(new Intent(DashboardActivity.this, LoginActivity.class));
                        finish();
                        Toast.makeText(DashboardActivity.this, "Logout Successfully", Toast.LENGTH_SHORT).show();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    void setupDrawerToggle()
    {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name);
        //This is necessary to change the icon of the Drawer Toggle upon state change.
        mDrawerToggle.syncState();
    }

    private void prepareListData() {
        mExpandableListTitle = new ArrayList<String>();
        mExpandableListData = new HashMap<String, List<String>>();

        // Adding child data
        mExpandableListTitle.add("Dashboard");
        mExpandableListTitle.add("Profile");
        mExpandableListTitle.add("Help");
        mExpandableListTitle.add("Withdrawal");
        mExpandableListTitle.add("E Pin");
        mExpandableListTitle.add("Direct User List");
        mExpandableListTitle.add("Direct Income Report");
        mExpandableListTitle.add("Support Center");
        mExpandableListTitle.add("Logout");

        // Adding child data
        List<String> epin = new ArrayList<String>();
        epin.add("E-Pin Details");
        epin.add("Used E-Pin Report");
        epin.add("Unused E-Pin");
        epin.add("Transfer E-Pin Report");
        epin.add("Received E-Pin Report");
       // epin.add("E-Pin Request Report");

//        List<String> incomeReport = new ArrayList<String>();
//        incomeReport.add("Distributor Income Report");
//        incomeReport.add("Silver Income Report");
//        incomeReport.add("Gold Income Report");
//        incomeReport.add("Platinum Income Report");
//        incomeReport.add("Diamond Income Report");
//        incomeReport.add("Repurchase Binary Income Report");
//
//        List<String> topUp = new ArrayList<String>();
//        topUp.add("Activate Account");
//        topUp.add("Activate Account Report");

//        List<String> withdrawal = new ArrayList<String>();
//        //withdrawal.add("Withdraw");
//        withdrawal.add("Withdraw Pending");
//        withdrawal.add("Withdraw Confirmed");

        List<String> help = new ArrayList<String>();
        help.add("Provide Help Report");
        help.add("Get Help Report");
        help.add("Link Details");

//        help.add("Help Details");

        List<String> withdrawal = new ArrayList<String>();
        withdrawal.add("Make Withdrawal");
        withdrawal.add("Withdrawal History");






        List<String> allTransactions = new ArrayList<String>();

        mExpandableListData.put(mExpandableListTitle.get(0), allTransactions);
        mExpandableListData.put(mExpandableListTitle.get(1), allTransactions);
        mExpandableListData.put(mExpandableListTitle.get(2), help);
        mExpandableListData.put(mExpandableListTitle.get(3), withdrawal);
        mExpandableListData.put(mExpandableListTitle.get(4), epin);
        mExpandableListData.put(mExpandableListTitle.get(5), allTransactions);
        mExpandableListData.put(mExpandableListTitle.get(6), allTransactions);
        mExpandableListData.put(mExpandableListTitle.get(7), allTransactions);
        mExpandableListData.put(mExpandableListTitle.get(8), allTransactions);
//        mExpandableListData.put(mExpandableListTitle.get(7), allTransactions);
//        mExpandableListData.put(mExpandableListTitle.get(8), business);
//        mExpandableListData.put(mExpandableListTitle.get(9), allTransactions);


    }

    void setupToolbar()
    {
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            switch (event.getAction()) {
                case KeyEvent.ACTION_DOWN:
                    if (event.getDownTime() - lastPressedTime < PERIOD) {
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "Press again to exit.",
                                Toast.LENGTH_SHORT).show();
                        lastPressedTime = event.getEventTime();
                    }
                    return true;
            }
        }
        return false;
    }
}
