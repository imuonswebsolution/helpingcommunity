package com.helping.community.app.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.helping.community.app.R;
import com.helping.community.app.models.SupportCenterDataModel;
import com.helping.community.app.models.SupportCenterResponseModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.ViewUtils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SupportCenterFragment extends Fragment {

    private EditText  edit_name, edit_email, edit_subject, edit_query;
    private Button button_submit;

    public SupportCenterFragment() {
        // Required empty public constructor
    }

    public static SupportCenterFragment newInstance() {
        SupportCenterFragment fragment = new SupportCenterFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_support_center, container, false);
        initializeViews(view);
        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateUserID()) {
                    registerListener();
                }
            }
        });
        return view;
    }
    private boolean validateUserID() {
        String name = edit_name.getText().toString().trim();
        String email = edit_email.getText().toString().trim();
        String subject = edit_subject.getText().toString().trim();
        String query = edit_query.getText().toString().trim();

        if (name.isEmpty()) {
            Toast.makeText(SupportCenterFragment.this.getContext(), getString(R.string.fullname), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (email.isEmpty() || !email.matches(Constants.EMAIL_REGEX)) {
            Toast.makeText(SupportCenterFragment.this.getContext(), getString(R.string.enteremail), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (subject.isEmpty()) {
            Toast.makeText(SupportCenterFragment.this.getContext(), getString(R.string.entersubject), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (query.isEmpty()) {
            Toast.makeText(SupportCenterFragment.this.getContext(), getString(R.string.enterquery), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    private void registerListener() {
        final ProgressDialog pd = ViewUtils.getProgressBar(SupportCenterFragment.this.getContext(),
                "Loading...", "Please wait..!");
        Map<String, String> loginMap = new HashMap<>();

        loginMap.put("email", edit_email.getText().toString().trim());
        loginMap.put("fullname", edit_name.getText().toString().trim());
        loginMap.put("message", edit_query.getText().toString().trim());
        loginMap.put("subject", edit_subject.getText().toString().trim());

        HelpingServices apiService = ApiHandler.getApiService();
        final Call<SupportCenterResponseModel> loginCall = apiService.wsSupport( "Bearer " + SharedPreferenceUtils.getLoginObject(
                SupportCenterFragment.this.getContext()).getData().getAccess_token(),loginMap);
        loginCall.enqueue(new Callback<SupportCenterResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<SupportCenterResponseModel> call,
                                   Response<SupportCenterResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    SupportCenterResponseModel SupportCenterResponseModel = response.body();
                    if (!SupportCenterResponseModel.getStatus().isEmpty() &&
                            SupportCenterResponseModel.getStatus().trim().equalsIgnoreCase("OK")) {

                        Toast.makeText(SupportCenterFragment.this.getContext(), "Your enquiry has been submitted successfully.",
                                Toast.LENGTH_SHORT).show();

                        edit_email.getText().clear();;
                        edit_name.getText().clear();
                        edit_query.getText().clear();
                        edit_subject.getText().clear();

                    } else {
                        Toast.makeText(SupportCenterFragment.this.getContext(), SupportCenterResponseModel.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<SupportCenterResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Log.e("ERROR", t.toString());
                Toast.makeText(SupportCenterFragment.this.getContext(), "Something went wrong..Please try again..!",
                        Toast.LENGTH_SHORT).show();

            }
        });
    }



    private void initializeViews(View view) {

        edit_name = view.findViewById(R.id.edit_name);
        edit_email = view.findViewById(R.id.edit_email);
        edit_subject = view.findViewById(R.id.edit_subject);
        edit_query = view.findViewById(R.id.edit_query);
        button_submit = view.findViewById(R.id.button_submit);
    }

}
