package com.helping.community.app.fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.helping.community.app.R;
import com.helping.community.app.adapter.UsedEPinHistoryAdapter;
import com.helping.community.app.models.UsedEPinDataModel;
import com.helping.community.app.models.UsedEPinResponseModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.Utils;
import com.helping.community.app.utils.ViewUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class UsedEPinHistoryFragment extends Fragment {

    private ListView mListUsedPinHistory;
    private static int mIndex = 0;
    private UsedEPinHistoryAdapter adapter;
    private List<UsedEPinDataModel> mHistoryList = new ArrayList<>();

    public static UsedEPinHistoryFragment newInstance() {
        UsedEPinHistoryFragment fragment = new UsedEPinHistoryFragment();
        return fragment;
    }

    public UsedEPinHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_used_epin_history, container, false);

        mIndex = 0;
        initializeViews(view);
        registerListeners();
        if (Utils.checkInternetConnection(UsedEPinHistoryFragment.this.getContext())) {
            getHistory();
        } else {
            Toast.makeText(UsedEPinHistoryFragment.this.getContext(),
                    getString(R.string.no_internet_connection_message), Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    private void registerListeners() {
        mListUsedPinHistory.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && (mListUsedPinHistory.getLastVisiblePosition() - mListUsedPinHistory.getHeaderViewsCount() -
                        mListUsedPinHistory.getFooterViewsCount()) >= (adapter.getCount() - 1)) {

                    if (Utils.checkInternetConnection(UsedEPinHistoryFragment.this.getContext())) {
                        getHistory();
                    } else {
                        Toast.makeText(UsedEPinHistoryFragment.this.getContext(), getString(R.string.no_internet_connection_message), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    private void getHistory() {
        final ProgressDialog pd = ViewUtils.getProgressBar(UsedEPinHistoryFragment.this.getContext(),
                "Loading...", "Please wait..!");
        Map<String, String> loginMap = new HashMap<>();

        loginMap.put("start", String.valueOf(mIndex));
        loginMap.put("length", "10");

        HelpingServices apiService = ApiHandler.getApiService();
        final Call<UsedEPinResponseModel> loginCall = apiService.wsUsedPinHistoryReport(
                "Bearer " + SharedPreferenceUtils.getLoginObject(
                        UsedEPinHistoryFragment.this.getContext()).getData().getAccess_token(),
                loginMap);
        loginCall.enqueue(new Callback<UsedEPinResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<UsedEPinResponseModel> call,
                                   Response<UsedEPinResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    UsedEPinResponseModel loginModel = response.body();
                    if (loginModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            loginModel.getStatus().equals("OK")) {
                        mHistoryList.addAll(loginModel.getData().getRecords());
                        adapter = new UsedEPinHistoryAdapter(UsedEPinHistoryFragment.this.getContext(), mHistoryList);
                        mListUsedPinHistory.setAdapter(adapter);
                        mListUsedPinHistory.setSelection(mIndex);
                        mIndex = mIndex + loginModel.getData().getRecords().size();
                    }
                }
            }

            @Override
            public void onFailure(Call<UsedEPinResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(UsedEPinHistoryFragment.this.getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initializeViews(View view) {
        mListUsedPinHistory = view.findViewById(R.id.list_used_pin_report);
    }
}
