package com.helping.community.app.views;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.helping.community.app.R;
import com.helping.community.app.models.ViewSlipResponseModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.ViewUtils;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewSlip extends AppCompatActivity {
    ImageView imageView;
    String transid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_slip);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("View slip");
        }
        imageView = findViewById(R.id.viewslip);
        Intent intent = getIntent();
        transid = intent.getStringExtra("transaction_id");
        registerListener();
    }

    private void registerListener() {
        final ProgressDialog pd = ViewUtils.getProgressBar(ViewSlip.this, "Loading...", "Please wait..!");



        Map<String, String> loginMap = new HashMap<>();


        loginMap.put("transaction_id", transid);

        HelpingServices apiService = ApiHandler.getApiService();

        final Call<ViewSlipResponseModel> loginCall = apiService.wsViewSlip(
                "Bearer " + SharedPreferenceUtils.getLoginObject(
                        ViewSlip.this).getData().getAccess_token(),loginMap);
        loginCall.enqueue(new Callback<ViewSlipResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<ViewSlipResponseModel> call,
                                   Response<ViewSlipResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    ViewSlipResponseModel viewSlipResponseModel = response.body();
                    if (viewSlipResponseModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            viewSlipResponseModel.getStatus().equals("OK")) {
                        String geturl = viewSlipResponseModel.getData().getAttachment();
                      Picasso.with(ViewSlip.this).load(geturl).into(imageView);

                        Toast.makeText(ViewSlip.this, viewSlipResponseModel.getMessage(), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(ViewSlip.this, viewSlipResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ViewSlipResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(ViewSlip.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //do whatever
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
