package com.helping.community.app.views;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.helping.community.app.R;
import com.helping.community.app.models.RegisterResponseModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;

import com.helping.community.app.utils.ViewUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {
    Button button_register;
    String epin;
    Spinner mSelectPH;
    EditText edit_user_name,edit_user_mobile_no,edit_email_id,edit_enter_password,edit_password_confrim,edit_pin,
            edit_account_holder_name,edit_account_number,edit_bank_name,edit_branch_name,edit_ifsc_code,
            edit_paytm_number,edit_googlepay_number,edit_phonepay_number,edit_mobikwik_number,edit_sponser_id;

    private ArrayList<String> mListPH;
    private String mStringRelation = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        epin  = getIntent().getStringExtra("epindetail");
        initializeViews();

        mSelectPH.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    mStringRelation = mListPH.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        setSpinnerAdapter();




button_register.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if (validateFullName() && validateMobileNo() && validateEmail()
                && validatePassword() && comfirmPassword() && validateConfirmPassword()
        && validateIfscCode() && validateBranchName() && validateBankName() && validateAccountName()
        && validateAccountNo()&&validateSponserId()) {
           register();
        }
    }
});

    }

    private void register() {
        final ProgressDialog pd = ViewUtils.getProgressBar(RegistrationActivity.this, "Loading...", "Please wait..!");

        Map<String, String> loginMap = new HashMap<>();
        final String user_name, user_mobile_no,email_id,enter_password,password_confrim,pin,
                account_holder_name,account_number,bank_name,branch_name,ifsc_code,paytm_number,
                googlepay_number,phonepay_number,mobikwik_number,sponser_id,ph_amount;


        sponser_id = edit_sponser_id.getText().toString().trim();
        user_name = edit_user_name.getText().toString().trim();
        user_mobile_no = edit_user_mobile_no.getText().toString().trim();
        email_id = edit_email_id.getText().toString().trim();
        enter_password= edit_enter_password.getText().toString().trim();
        password_confrim = edit_password_confrim.getText().toString().trim();
        pin = edit_pin.getText().toString().trim();
        account_holder_name = edit_account_holder_name.getText().toString().trim();
        account_number = edit_account_number.getText().toString().trim();
        bank_name = edit_bank_name.getText().toString().trim();
        branch_name = edit_branch_name.getText().toString().trim();
        ifsc_code = edit_ifsc_code.getText().toString().trim();
        paytm_number = edit_paytm_number.getText().toString().trim();
        googlepay_number = edit_googlepay_number.getText().toString().trim();
        phonepay_number = edit_phonepay_number.getText().toString().trim();
        mobikwik_number = edit_mobikwik_number.getText().toString().trim();
        ph_amount =mSelectPH.getSelectedItem().toString();



        loginMap.put("ref_user_id", sponser_id);
        loginMap.put("account_no", account_number);
        loginMap.put("bank_name", bank_name);
        loginMap.put("branch_name", branch_name);
        loginMap.put("country", "IN");
        loginMap.put("email", email_id);
        loginMap.put("fullname", user_name);
        loginMap.put("holder_name", account_holder_name);
        loginMap.put("ifsc_code", ifsc_code);
        loginMap.put("mobikwik_no",mobikwik_number);
        loginMap.put("mobile",user_mobile_no);
        loginMap.put("password", enter_password);
        loginMap.put("password_confirmation", password_confrim);
        loginMap.put("paytm_no",paytm_number);
        loginMap.put("phonepe_no",phonepay_number );
        loginMap.put("pin_number", pin);
        loginMap.put("tez_no", googlepay_number);
        loginMap.put("user_id", "");
        loginMap.put("amount", ph_amount);

        HelpingServices apiService = ApiHandler.getApiService();
        final Call<RegisterResponseModel> loginCall = apiService.wsRegister(loginMap);
        loginCall.enqueue(new Callback<RegisterResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<RegisterResponseModel> call,
                                   Response<RegisterResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    RegisterResponseModel registerResponseModel = response.body();
                    if (registerResponseModel.getCode() == Constants.RESPONSE_CODE &&
                            registerResponseModel.getStatus().equals("Created")) {
                       String useridtosend = registerResponseModel.getRegisterDataModel().getUserId();
                       Intent thanku = new  Intent (RegistrationActivity.this,ThankYou.class);
                       thanku.putExtra("userid",useridtosend);
                       startActivity(thanku);


                    } else {
                        Toast.makeText(RegistrationActivity.this, registerResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(RegistrationActivity.this, "Check username or password", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(RegistrationActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validateConfirmPassword() {
        String password = edit_enter_password.getText().toString().trim();
        String confirmPassword = edit_password_confrim.getText().toString().trim();
        if (!confirmPassword.equals(password)) {
            Toast.makeText(RegistrationActivity.this, getString(R.string.invalid_confirm_password_message), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean comfirmPassword() {
        String comfirmpassword = edit_enter_password.getText().toString().trim();
        if (comfirmpassword.isEmpty() && comfirmpassword.length() > 8) {
            Toast.makeText(RegistrationActivity.this, getString(R.string.empty_password), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validatePassword() {
        String password = edit_password_confrim.getText().toString().trim();
        if (password.isEmpty() && password.length() > 8) {
            Toast.makeText(RegistrationActivity.this, getString(R.string.empty_password), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateEmail() {
        String email = edit_email_id.getText().toString().trim();
        if (email.isEmpty() || !email.matches(Constants.EMAIL_REGEX)) {
            Toast.makeText(RegistrationActivity.this,
                    getString(R.string.invalid_email_message), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateMobileNo() {
        String mobile = edit_user_mobile_no.getText().toString().trim();
        if (mobile.isEmpty() || mobile.length() < 10) {
            Toast.makeText(RegistrationActivity.this,
                    getString(R.string.invalid_mobile_number_message), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateFullName() {
        String fullName = edit_user_name.getText().toString().trim();
        if (fullName.isEmpty()) {
            Toast.makeText(RegistrationActivity.this,
                    getString(R.string.invalid_full_name_message), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateIfscCode() {
        String ifsc = edit_ifsc_code.getText().toString().trim();
        if (ifsc.isEmpty() && ifsc.length() > 5) {
            Toast.makeText(RegistrationActivity.this, getString(R.string.empty_ifsc_code), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    private boolean validateBranchName() {
        String branchName = edit_branch_name.getText().toString().trim();
        if (branchName.isEmpty()) {
            Toast.makeText(RegistrationActivity.this, getString(R.string.empty_branch_name), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateBankName() {
        String bankName = edit_bank_name.getText().toString().trim();
        if (bankName.isEmpty()) {
            Toast.makeText(RegistrationActivity.this, getString(R.string.empty_bank_name), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateAccountName() {
        String accountName = edit_account_holder_name.getText().toString().trim();
        if (accountName.isEmpty()) {
            Toast.makeText(RegistrationActivity.this, getString(R.string.empty_account_name), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateAccountNo() {
        String accountNo = edit_account_number.getText().toString().trim();
        if (accountNo.isEmpty()) {
            Toast.makeText(RegistrationActivity.this, getString(R.string.empty_account_number), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateSponserId() {
        String sponserId = edit_sponser_id.getText().toString().trim();
        if (sponserId.isEmpty()) {
            Toast.makeText(RegistrationActivity.this, getString(R.string.empty_sponsor_id), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    private void setSpinnerAdapter() {
        mListPH = new ArrayList<String>();

        List<String> ph = new ArrayList<>();
        ph.add("Select PH");
        ph.add("8000");
        ph.add("16000");

        mListPH.addAll(ph);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(RegistrationActivity.this,
                R.layout.layout_spinner_item, ph);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSelectPH.setAdapter(spinnerArrayAdapter);
    }


    private void initializeViews() {
        edit_user_name = findViewById(R.id.edit_user_name);
        edit_user_mobile_no = findViewById(R.id.edit_user_mobile_no);
        edit_email_id = findViewById(R.id.edit_email_id);
        edit_enter_password = findViewById(R.id.edit_enter_password);
        edit_password_confrim = findViewById(R.id.edit_password_confrim);
        edit_pin = findViewById(R.id.edit_pin);
        edit_account_holder_name = findViewById(R.id.edit_account_holder_name);
        edit_account_number = findViewById(R.id.edit_account_number);
        edit_bank_name = findViewById(R.id.edit_bank_name);
        edit_branch_name = findViewById(R.id.edit_branch_name);
        edit_ifsc_code = findViewById(R.id.edit_ifsc_code);
        edit_paytm_number = findViewById(R.id.edit_paytm_number);
        edit_googlepay_number = findViewById(R.id.edit_googlepay_number);
        edit_phonepay_number = findViewById(R.id.edit_phonepay_number);
        edit_mobikwik_number = findViewById(R.id.edit_mobikwik_number);
        button_register = findViewById(R.id.button_register);
        edit_sponser_id = findViewById(R.id.edit_sponser_id);
        edit_pin.setText(epin);
        mSelectPH =  findViewById(R.id.select_ph);
    }

}
