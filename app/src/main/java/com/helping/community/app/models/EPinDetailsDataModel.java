package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class EPinDetailsDataModel implements Serializable {

    @SerializedName("recordsTotal")
    @Expose
    public Integer recordsTotal;
    @SerializedName("recordsFiltered")
    @Expose
    public Integer recordsFiltered;
    @SerializedName("records")
    @Expose
    public List<EPinDetailsRecordsModel> records = null;

    public Integer getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(Integer recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public Integer getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(Integer recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public List<EPinDetailsRecordsModel> getRecords() {
        return records;
    }

    public void setRecords(List<EPinDetailsRecordsModel> records) {
        this.records = records;
    }
}
