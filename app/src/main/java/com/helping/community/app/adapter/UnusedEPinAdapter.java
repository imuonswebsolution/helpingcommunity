package com.helping.community.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.helping.community.app.R;
import com.helping.community.app.models.EPinDetailsRecordsModel;

import java.util.ArrayList;
import java.util.List;

public class UnusedEPinAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    List<EPinDetailsRecordsModel> mHistoryList;

    public UnusedEPinAdapter(Context context, List<EPinDetailsRecordsModel> list) {
        mContext = context;
        mHistoryList = new ArrayList<EPinDetailsRecordsModel>();
        mHistoryList.addAll(list);
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mHistoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.layout_history_list_item, null);
        }
        holder = new ViewHolder();
        final EPinDetailsRecordsModel details = mHistoryList.get(position);
if(details.getStatus().equals("Active") ) {
    holder.textProductName = view.findViewById(R.id.text_amount);
    holder.textUserDate = view.findViewById(R.id.text_date);
    holder.textEPIN = view.findViewById(R.id.text_payout_no);
    holder.textStatus = view.findViewById(R.id.text_details);

    holder.textProductName.setText(details.getProduct_name());
    holder.textUserDate.setText(details.getTransfer_date().split(" ")[0]);
    holder.textEPIN.setText(details.getPin());
    holder.textStatus.setText(details.getStatus());
}
        if (details.getStatus().equalsIgnoreCase("Active")) {
            holder.textStatus.setTextColor(Color.parseColor("#FF276327"));
        } else {
            holder.textStatus.setTextColor(Color.parseColor("#ff0000"));
        }

        return view;
    }

    public static class ViewHolder {
        TextView textEPIN, textProductName, textUserDate, textStatus;
    }
}
