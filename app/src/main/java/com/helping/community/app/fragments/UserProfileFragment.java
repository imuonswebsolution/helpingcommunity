package com.helping.community.app.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.helping.community.app.R;
import com.helping.community.app.models.UserProfileDataModel;
import com.helping.community.app.models.UserProfileResponseModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.ViewUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UserProfileFragment extends Fragment {

    private TextView mTextUserId, mTextFullname, mTextSponsorId, mTextEmailId, mTextNumber, mTextCountry, mTextBtcAdd;


    public UserProfileFragment() {
        // Required empty public constructor
    }

    public static UserProfileFragment newInstance() {
        UserProfileFragment fragment = new UserProfileFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        initializeViews(view);
        registerListener();
        return view;
    }

    private void registerListener() {
        final ProgressDialog pd = ViewUtils.getProgressBar(UserProfileFragment.this.getContext(), "Loading...", "Please wait..!");
        HelpingServices apiService = ApiHandler.getApiService();
        final Call<UserProfileResponseModel> loginCall = apiService.wsUserProfileInfo(
                "Bearer " +  SharedPreferenceUtils.getAccesstoken(UserProfileFragment.this.getContext()));
        loginCall.enqueue(new Callback<UserProfileResponseModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<UserProfileResponseModel> call,
                                   Response<UserProfileResponseModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    UserProfileResponseModel responseModel = response.body();
                    if (responseModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            responseModel.getStatus().equals("OK")) {
                       // Toast.makeText(UserProfileFragment.this.getContext(), responseModel.getMessage(), Toast.LENGTH_SHORT).show();

                        setData(responseModel.getUserProfileDataModel());
                    } else {
                        Toast.makeText(UserProfileFragment.this.getContext(), responseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserProfileResponseModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(UserProfileFragment.this.getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void setData(UserProfileDataModel userProfileDataModel) {

        mTextUserId.setText(userProfileDataModel.getUserId());
        mTextFullname.setText(userProfileDataModel.getFullname());
//        mTextSponsorId.setText(userProfileDataModel.gete());
        mTextEmailId.setText(userProfileDataModel.getEmail());
        mTextNumber.setText(userProfileDataModel.getMobile());
//        mTextCountry.setText(userProfileDataModel.getCountry());
//        mTextBtcAdd.setText(userProfileDataModel.getBtcAddress());


    }

    private void initializeViews(View view) {

        mTextUserId = view.findViewById(R.id.text_UP_userId);
        mTextFullname = view.findViewById(R.id.text_UP_fullname);
        mTextSponsorId = view.findViewById(R.id.text_UP_sponsorId);
        mTextEmailId = view.findViewById(R.id.text_UP_emailId);
        mTextNumber = view.findViewById(R.id.text_UP_Number);
        mTextCountry = view.findViewById(R.id.text_UP_country);
        mTextBtcAdd = view.findViewById(R.id.text_UP_btcadd);
    }

}
