package com.helping.community.app.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.helping.community.app.R;
import com.helping.community.app.models.Datum;
import com.helping.community.app.models.LinkDetailsModel;

import com.helping.community.app.models.HelpDetailsModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.ViewUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HelpDetailsFragment extends Fragment {

    private TextView mTextFromUser, mTextAmount, mTextToUser, mTextGooglePay, mTextPaytm, mTextPhonepe, mTextMobikwik, mTextHolderName,
            mTextAccountNumber, mTextBranchName, mTextIfscCode;


    public HelpDetailsFragment() {
        // Required empty public constructor
    }

    public static HelpDetailsFragment newInstance() {
        HelpDetailsFragment fragment = new HelpDetailsFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.layout_help_details, container, false);
        initializeViews(view);
        registerListener();
        return view;
    }

    private void registerListener() {
        final ProgressDialog pd = ViewUtils.getProgressBar(HelpDetailsFragment.this.getContext(), "Loading...", "Please wait..!");


//        String transaction_id = linkDetailsModel.getData().getRecords().getT
        Map<String, String> loginMap = new HashMap<>();

        loginMap.put("network_type", "BTC");
        loginMap.put("transaction_id", "8417251401");

        HelpingServices apiService = ApiHandler.getApiService();

        final Call<HelpDetailsModel> loginCall = apiService.wsLinkHelpDetails(
                "Bearer " + SharedPreferenceUtils.getLoginObject(
                        HelpDetailsFragment.this.getContext()).getData().getAccess_token(),loginMap);
        loginCall.enqueue(new Callback<HelpDetailsModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<HelpDetailsModel> call,
                                   Response<HelpDetailsModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    HelpDetailsModel responseModel = response.body();
                    if (responseModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            responseModel.getStatus().equals("OK")) {
                        Toast.makeText(HelpDetailsFragment.this.getContext(), responseModel.getMessage(), Toast.LENGTH_SHORT).show();

                        setData(responseModel.getData());
                    } else {
                        Toast.makeText(HelpDetailsFragment.this.getContext(), responseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<HelpDetailsModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(HelpDetailsFragment.this.getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void setData(List<Datum> data) {
        for (int i = 0; i < data.size(); i++) {
            mTextFromUser.setText(data.get(i).getFromUserid().toString());
            mTextAmount.setText(data.get(i).getAmount().toString());
            mTextToUser.setText(data.get(i).getToUserid().toString());
            mTextGooglePay.setText(data.get(i).getToTezNo().toString());
            if(data.get(i).getToPaytmNo()  != null){
                mTextPaytm.setText(data.get(i).getToPaytmNo().toString());
            }else{
                mTextPaytm.setText("");
            }
            if(data.get(i).getToPhonepeNo()  != null){
                mTextPhonepe.setText(data.get(i).getToPhonepeNo().toString());
            }else{
                mTextPhonepe.setText("");
            }

            mTextHolderName.setText(data.get(i).getToHolderName().toString());
            mTextAccountNumber.setText(data.get(i).getToAccountNo().toString());
            mTextBranchName.setText(data.get(i).getToBranchName().toString());
            mTextIfscCode.setText(data.get(i).getToIfscCode().toString());
        }




    }

    private void initializeViews(View view) {

        mTextFromUser = view.findViewById(R.id.text_from_user);
        mTextAmount = view.findViewById(R.id.text_amount);
        mTextToUser = view.findViewById(R.id.text_to_user);
        mTextGooglePay = view.findViewById(R.id.text_google_pay);
        mTextPaytm = view.findViewById(R.id.text_paytm_number);
        mTextPhonepe = view.findViewById(R.id.text_phone_pe_number);
        mTextMobikwik = view.findViewById(R.id.text_mobikwik_number);
        mTextHolderName = view.findViewById(R.id.text_user_holder_name);
        mTextAccountNumber = view.findViewById(R.id.text_user_account_number);
        mTextBranchName = view.findViewById(R.id.text_branch_name);
        mTextIfscCode = view.findViewById(R.id.text_ifsc_code);

    }

}
