package com.helping.community.app.views;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.helping.community.app.R;
import com.helping.community.app.models.Datum;
import com.helping.community.app.models.HelpDetailsModel;
import com.helping.community.app.models.LinkDetailsModel;
import com.helping.community.app.retrofit.ApiHandler;
import com.helping.community.app.retrofit.HelpingServices;
import com.helping.community.app.utils.Constants;
import com.helping.community.app.utils.SharedPreferenceUtils;
import com.helping.community.app.utils.ViewUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HelpDetailsActivity extends AppCompatActivity {

    private TextView mTextFromUser, mTextAmount, mTextToUser, mTextGooglePay, mTextPaytm, mTextPhonepe, mTextMobikwik, mTextHolderName,
            mTextAccountNumber, mTextBranchName, mTextIfscCode;
    String transid;

    public HelpDetailsActivity() {
        // Required empty public constructor
    }

    public static HelpDetailsActivity newInstance() {
        HelpDetailsActivity fragment = new HelpDetailsActivity();
        return fragment;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_help_details);
        initializeViews();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Help Details");
        }


        Intent intent = getIntent();
        transid = intent.getStringExtra("transaction_id");
        registerListener();

    }






    private void registerListener() {
        final ProgressDialog pd = ViewUtils.getProgressBar(HelpDetailsActivity.this, "Loading...", "Please wait..!");
LinkDetailsModel linkDetailsModel = new LinkDetailsModel();

//        String transaction_id = linkDetailsModel.getData().getRecords().getT
        Map<String, String> loginMap = new HashMap<>();

        loginMap.put("network_type", "BTC");
        loginMap.put("transaction_id", transid);

        HelpingServices apiService = ApiHandler.getApiService();

        final Call<HelpDetailsModel> loginCall = apiService.wsLinkHelpDetails(
                "Bearer " + SharedPreferenceUtils.getLoginObject(
                        HelpDetailsActivity.this).getData().getAccess_token(),loginMap);
        loginCall.enqueue(new Callback<HelpDetailsModel>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<HelpDetailsModel> call,
                                   Response<HelpDetailsModel> response) {
                pd.hide();
                if (response.isSuccessful()) {
                    HelpDetailsModel responseModel = response.body();
                    if (responseModel.getCode() == Constants.RESPONSE_CODE_OK &&
                            responseModel.getStatus().equals("OK")) {
                        Toast.makeText(HelpDetailsActivity.this, responseModel.getMessage(), Toast.LENGTH_SHORT).show();

                        setData(responseModel.getData());
                    } else {
                        Toast.makeText(HelpDetailsActivity.this, responseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<HelpDetailsModel> call,
                                  Throwable t) {
                pd.hide();
                Toast.makeText(HelpDetailsActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void setData(List<Datum> data) {
        for (int i = 0; i < data.size(); i++) {
            if(data.get(i).getFromUserid() != null) {
                mTextFromUser.setText(data.get(i).getFromUserid().toString());
            }

            if(data.get(i).getAmount()!= null) {

                mTextAmount.setText(data.get(i).getAmount().toString());
            }

            if(data.get(i).getToUserid() != null) {
                mTextToUser.setText(data.get(i).getToUserid().toString());
            }


            if(data.get(i).getToTezNo() != null) {
                mTextGooglePay.setText(data.get(i).getToTezNo().toString());
            }

            if(data.get(i).getToPaytmNo()  != null){
                mTextPaytm.setText(data.get(i).getToPaytmNo().toString());
            }else{
                mTextPaytm.setText("");
            }
            if(data.get(i).getToPhonepeNo()  != null){
                mTextPhonepe.setText(data.get(i).getToPhonepeNo().toString());
            }else{
                mTextPhonepe.setText("");
            }

            if(data.get(i).getToHolderName()!= null) {
                mTextHolderName.setText(data.get(i).getToHolderName().toString());
            }

            if(data.get(i).getToAccountNo()!= null) {
                mTextAccountNumber.setText(data.get(i).getToAccountNo().toString());
            }

            if(data.get(i).getToBranchName()!= null) {
                mTextBranchName.setText(data.get(i).getToBranchName().toString());
            }

            if(data.get(i).getToIfscCode()!= null) {
                mTextIfscCode.setText(data.get(i).getToIfscCode().toString());
            }
        }




    }

    private void initializeViews() {

        mTextFromUser = findViewById(R.id.text_from_user);
        mTextAmount = findViewById(R.id.text_amount);
        mTextToUser = findViewById(R.id.text_to_user);
        mTextGooglePay = findViewById(R.id.text_google_pay);
        mTextPaytm = findViewById(R.id.text_paytm_number);
        mTextPhonepe = findViewById(R.id.text_phone_pe_number);
        mTextMobikwik = findViewById(R.id.text_mobikwik_number);
        mTextHolderName = findViewById(R.id.text_user_holder_name);
        mTextAccountNumber = findViewById(R.id.text_user_account_number);
        mTextBranchName = findViewById(R.id.text_branch_name);
        mTextIfscCode = findViewById(R.id.text_ifsc_code);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //do whatever
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
