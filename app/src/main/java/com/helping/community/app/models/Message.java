package com.helping.community.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Message {
    @SerializedName("showtime")
    @Expose
    private String showtime;
    @SerializedName("msgdata")
    @Expose
    private List<Msgdatum_> msgdata = null;

    public String getShowtime() {
        return showtime;
    }

    public void setShowtime(String showtime) {
        this.showtime = showtime;
    }

    public List<Msgdatum_> getMsgdata() {
        return msgdata;
    }

    public void setMsgdata(List<Msgdatum_> msgdata) {
        this.msgdata = msgdata;
    }
}
